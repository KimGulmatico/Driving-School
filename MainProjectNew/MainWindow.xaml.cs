﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using System.Configuration;
using System.Data;
using AForge.Video.DirectShow;
using AForge.Video;
using System.Windows.Media.Imaging;
using System.IO;
using System.Threading;
using System.Drawing.Imaging;
using System.Drawing;

namespace MainProjectNew
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int GWL_STYLE = -16;
        private const int WS_SYSMENU = 0x80000;
        [DllImport("user32.dll", SetLastError = true)]
        private static extern int GetWindowLong(IntPtr hWnd, int nIndex);
        [DllImport("user32.dll")]
        private static extern int SetWindowLong(IntPtr hWnd, int nIndex, int dwNewLong);
        System.Windows.Threading.DispatcherTimer Timer = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer Timer2 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer Timer3 = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer TimerHome = new System.Windows.Threading.DispatcherTimer();
        System.Windows.Threading.DispatcherTimer Clock = new System.Windows.Threading.DispatcherTimer();

        //SQL
        public SqlConnection con = new SqlConnection(@"Server = KIMGULMATICO\SQLEXPRESS01;
                                                Initial Catalog = HVNds;
                                                Integrated Security = true;");
        public SqlCommand cmd;
        public SqlDataReader rdr;
        public SqlDataAdapter sda;

        //Data Tables
        DataTable dtStudents;
        DataTable dtStudentsPay;
        DataTable dtTransactions;
        DataTable dtCncledPay;
        DataTable dtInstructors;
        DataTable dtCars;
        DataTable dtStudSched;
        DataTable dtInstructorSched;
        DataTable dtCarsSched;
        DataTable dtScheduleAssign;
        DataTable dtSchedConflict;
        DataTable dtStudSchedStat;
        DataTable dtScheduleComplt;
        DataTable dtScheduleAbsent;
        DataTable dtHome;

        //Forms
        Print print;

        //backup SQL
        //string differential = @"BACKUP DATABASE [HVNds] TO  DISK = N'C:\Users\TOSHIBA intel\Dropbox\HVNds.bak' WITH  DIFFERENTIAL , NOFORMAT, NOINIT, SKIP, NOREWIND, NOUNLOAD,  STATS = 10,";
        string differential = @"BACKUP DATABASE [HVNds] TO  DISK = N'C:\Users\DSbackup\HVNds.bak' WITH  DIFFERENTIAL , NOFORMAT, NOINIT, SKIP, NOREWIND, NOUNLOAD,  STATS = 10,";
        //NAME = N'HVNds-Differential Database Backup'
        public MainWindow()
        {
            InitializeComponent();
            //print = new Print(this);
            Loaded += new RoutedEventHandler(MainWindow_Loaded);
            Timer.Tick += Timer_Tick;
            Timer2.Tick += Timer2_Tick;
            Timer3.Tick += Timer3_Tick;
            TimerHome.Tick += TimerHome_Tick;
            Clock.Tick += Clock_Tick;
            Timer.Interval = new TimeSpan(0, 0, 0,0,0);
            Timer2.Interval = new TimeSpan(0, 0, 0, 0, 0);
            Timer3.Interval = new TimeSpan(0, 0, 0, 0, 0);
            TimerHome.Interval = new TimeSpan(0, 0, 0, 0, 0);
            Clock.Interval = new TimeSpan(0, 0, 0, 1, 0);

        }
        void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Style _style = null;
            //if (Microsoft.Windows.Shell.SystemParameters2.Current.IsGlassEnabled == true)
            //{
            //    _style = (Style)Resources["MyStyle"];
            //}
            _style = (Style)Resources["MyStyle"];
            this.Style = _style;
            var hwnd = new WindowInteropHelper(this).Handle;
            SetWindowLong(hwnd, GWL_STYLE, GetWindowLong(hwnd, GWL_STYLE) & ~WS_SYSMENU);
            this.MinHeight = this.Height;
            this.MinWidth = this.Width;
            this.MaxHeight = SystemParameters.VirtualScreenHeight+10;
            this.MaxWidth = SystemParameters.VirtualScreenWidth+10;
            tDateFill.Text = DateTime.Now.ToShortDateString();
            addItemYear();
            addItemDay();
            cbSelectedS();
            cbSelectedI();
            loadDataStudents();
            loadDataStudentsPay();
            loadDataInstructors();
            loadDataCars();
            loadDataStudIns();
            loadDataStudStatSched();
            cbAssignFilter.SelectedIndex = 0;
            dtSchedDate.BlackoutDates.Add(new CalendarDateRange(new DateTime(0001, 1, 1),DateTime.Now.AddDays(-1)));
            if (DateTime.Now.DayOfWeek.ToString() == "Monday" || DateTime.Now.DayOfWeek.ToString() == "Friday")
            {
                //string path = @"C:\Users\TOSHIBA intel\Dropbox\HVNds.bak";
                string path = @"C:\Users\DSbackup\HVNds.bak";
                if (File.GetLastWriteTime(path).ToShortDateString() != DateTime.Now.ToShortDateString())
                {
                    //con.Open();
                    //cmd = new SqlCommand(@"BACKUP DATABASE [HVNds] TO  DISK = N'C:\Users\TOSHIBA intel\Dropbox\HVNds.bak' WITH NOFORMAT, NOINIT,  NAME = N'HVNds-Full Database Backup', SKIP, NOREWIND, NOUNLOAD,  STATS = 10", con);
                    //cmd.ExecuteNonQuery();
                    //con.Close();
                }
            }
       

        }
        private void addItemYear()
        {
            cbYear.Items.Add("Year");
            int y = DateTime.Now.Year + 1;
            while(y != 1910)
            {
                y--;
                cbYear.Items.Add(y.ToString());
                
            }
            iYr.ItemsSource = cbYear.Items;
        }
        private void addItemDay()
        {
            cbDay.Items.Add("Day");
            int d = 0;
            while (d != 31)
            {
                d++;
                cbDay.Items.Add(d.ToString());
            }
            iDay.ItemsSource = cbDay.Items;
        }
        private void cbSelectedS()
        {
            cbMth.SelectedIndex = 0;
            cbDay.SelectedIndex = 0;
            cbYear.SelectedIndex = 0;
        }
        private void cbSelectedI()
        {
            iMnth.SelectedIndex = 0;
            iDay.SelectedIndex = 0;
            iYr.SelectedIndex = 0;
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void button1_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            this.WindowState = System.Windows.WindowState.Minimized;
        }
        double multiply;
        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            
        }
       
        bool slidePlus;
        bool slideMinus;
        bool L, R;
        double selectedWidth;
        double multiplier;
        double selectedEntity;
        public void Timer_Tick(object sender, EventArgs e)
        {
            if (slideMinus)
            {
                slidePlus = false;
                if (EntityGrid.Width < selectedWidth + 20)
                {
                    if (EntityGrid.Width == selectedWidth)
                    {
                        Timer.Interval = new TimeSpan(0, 0, 0, 0, 0);
                        this.Timer.IsEnabled = false;
                        slideMinus = false;
                        selectedEntity = selectedWidth;
                    }
                    else
                    {
                        EntityGrid.Width -= 1;
                    }
                }
                else
                {
                    this.EntityGrid.Width -= 50;
                }
                
            }
            if(slidePlus)
            {
                slideMinus = false;
                if (EntityGrid.Width > selectedWidth - 20)
                {
                    if (this.EntityGrid.Width == selectedWidth)
                    {
                        Timer.Interval = new TimeSpan(0, 0, 0, 0, 0);
                        this.Timer.IsEnabled = false;
                        slidePlus = false;
                    }
                    else
                    {
                        EntityGrid.Width += 1;
                    }
                }
                else
                {
                    this.EntityGrid.Width += 50;
                }          
            }
        }
        double sliderRectLeft;
        double newMarginLeft;
        Label newlabel;
        Label newlabelWhite;
        public void Timer2_Tick(object sender, EventArgs e)
        {
            if (L)
            {
                if (this.redRect.Margin.Left < (sliderRectLeft + 10))
                {
                    if (this.redRect.Margin.Left == sliderRectLeft)
                    {
                        Timer2.IsEnabled = false;
                        L = false;
                        if (newlabelWhite != null)
                        {
                            newlabelWhite.Visibility = System.Windows.Visibility.Visible;
                        }
                    }
                    else
                    {
                        newMarginLeft -= 1;
                        redRect.Margin = new Thickness(newMarginLeft, 77, 0,0);
                        redRect.Width = newlabel.Width - 11;
                    }
                }
                else
                {

                    newMarginLeft = redRect.Margin.Left;
                    newMarginLeft -= 10;
                    redRect.Margin = new Thickness(newMarginLeft, 77, 0, 0);

                }
            }
            if (R)
            {
                if (this.redRect.Margin.Left > (sliderRectLeft - 10))
                {
                    if (this.redRect.Margin.Left == sliderRectLeft)
                    {
                        Timer2.IsEnabled = false;
                        R = false;
                        if (newlabelWhite != null)
                        {
                            newlabelWhite.Visibility = System.Windows.Visibility.Visible;
                        }
                    }
                    else
                    {
                        newMarginLeft += 1;
                        redRect.Margin = new Thickness(newMarginLeft, 77, 0,0);
                        redRect.Width = newlabel.Width - 11;
                    }
                }
                else
                {
                    newMarginLeft = redRect.Margin.Left;
                    newMarginLeft += 10;
                    redRect.Margin = new Thickness(newMarginLeft, 77, 0, 0);
                }
            }
        }
        private void checkMultiplier()
        {
            if (selectedWidth > EntityGrid.Width)
            {
                multiplier = (selectedWidth - EntityGrid.Width) / 1210;
            }
            else
            {
                multiplier = (EntityGrid.Width - selectedWidth) / 1210;
            }
       
        }
        private void label1_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if(gridHome.Visibility == System.Windows.Visibility.Visible)
            {
                gridHomeSlide.Width = 594;
                gridHome.Visibility = System.Windows.Visibility.Hidden;
                redRect.Visibility = System.Windows.Visibility.Visible;
                Clock.IsEnabled = false;
            }
            if (!Timer.IsEnabled && EntityGrid.Width != 1210 && !Timer2.IsEnabled)
            {
                if (EntityGrid.Width < 1210)
                {
                    slidePlus = true;
                }
                else
                {
                    L = true;
                    EntityGrid.Width = 2420;
                    slideMinus = true;
                }

                selectedWidth = 1210;
                sliderRectLeft = 160;
                newlabel = lStud;
                newlabelWhite = new Label();
                newlabelWhite = label1;
                multiply = 1;
                Timer.IsEnabled = true;
                Timer2.IsEnabled = true;
                label2.Visibility = System.Windows.Visibility.Hidden;
                label3.Visibility = System.Windows.Visibility.Hidden;
                label4.Visibility = System.Windows.Visibility.Hidden;
                label5.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void lSched_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (gridHome.Visibility == System.Windows.Visibility.Visible)
            {
                gridHomeSlide.Width = 594;
                gridHome.Visibility = System.Windows.Visibility.Hidden;
                redRect.Visibility = System.Windows.Visibility.Visible;
                Clock.IsEnabled = false;
            }
            if (!Timer.IsEnabled && EntityGrid.Width != 2420)
            {
                if (EntityGrid.Width < 2420)
                {
                    R = true;
                    slidePlus = true;
                }
                else
                {
                    L= true;
                    EntityGrid.Width = 3630;
                    slideMinus = true;
                }
                selectedWidth = 1210*2;
                sliderRectLeft = 296;
                newlabel = lSched;
                newlabelWhite = new Label();
                newlabelWhite = label2;
                multiply = 2;
                Timer.IsEnabled = true;
                Timer2.IsEnabled = true;
                label1.Visibility = System.Windows.Visibility.Hidden;
                label5.Visibility = System.Windows.Visibility.Hidden;
                label3.Visibility = System.Windows.Visibility.Hidden;
                label4.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void lPay_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (gridHome.Visibility == System.Windows.Visibility.Visible)
            {
                gridHomeSlide.Width = 594;
                gridHome.Visibility = System.Windows.Visibility.Hidden;
                redRect.Visibility = System.Windows.Visibility.Visible;
                Clock.IsEnabled = false;
            }
            if (!Timer.IsEnabled && EntityGrid.Width != 3630)
            {
                if (EntityGrid.Width < 3630)
                {
                    EntityGrid.Width = 2420;
                    R = true;
                    slidePlus = true;
                }
                else
                {
                    L = true;
                    EntityGrid.Width = 4840;
                    slideMinus = true;
                }
                selectedWidth = 1210*3;
                sliderRectLeft = 434;
                newlabel = lPay;
                newlabelWhite = new Label();
                newlabelWhite = label3;
                multiply = 3;
                Timer.IsEnabled = true;
                Timer2.IsEnabled = true;
                label1.Visibility = System.Windows.Visibility.Hidden;
                label2.Visibility = System.Windows.Visibility.Hidden;
                label5.Visibility = System.Windows.Visibility.Hidden;
                label4.Visibility = System.Windows.Visibility.Hidden;
                //checkMultiplier();
            }
        }

        private void lIns_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (gridHome.Visibility == System.Windows.Visibility.Visible)
            {
                gridHomeSlide.Width = 594;
                gridHome.Visibility = System.Windows.Visibility.Hidden;
                redRect.Visibility = System.Windows.Visibility.Visible;
                Clock.IsEnabled = false;
            }
            if (!Timer.IsEnabled && EntityGrid.Width != 4840)
            {
                if (EntityGrid.Width < 4840)
                {
                    R = true;
                    EntityGrid.Width = 3630;
                    slidePlus = true;
                }
                else
                {
                    L = true;
                    EntityGrid.Width = 6050;
                    slideMinus = true;
                }
                selectedWidth = 1210*4;
                sliderRectLeft = 573;
                newlabel = lIns;
                newlabelWhite = new Label();
                newlabelWhite = label4;
                multiply = 4;
                Timer.IsEnabled = true;
                Timer2.IsEnabled = true;
                label1.Visibility = System.Windows.Visibility.Hidden;
                label2.Visibility = System.Windows.Visibility.Hidden;
                label3.Visibility = System.Windows.Visibility.Hidden;
                label5.Visibility = System.Windows.Visibility.Hidden;
            }
           
        }

        private void lCars_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (gridHome.Visibility == System.Windows.Visibility.Visible)
            {
                gridHomeSlide.Width = 594;
                gridHome.Visibility = System.Windows.Visibility.Hidden;
                redRect.Visibility = System.Windows.Visibility.Visible;
                Clock.IsEnabled = false;
            }
            if (!Timer.IsEnabled && EntityGrid.Width != 6050)
            {
                if (EntityGrid.Width < 6050)
                {
                    R = true;
                    EntityGrid.Width = 4840;
                    slidePlus = true;
                }
                else
                {
                    L = true;
                    slideMinus = true;
                }

                selectedWidth = 1210*5;
                sliderRectLeft = 711;
                newlabel = lCars;
                newlabelWhite = new Label();
                newlabelWhite = label5;
                multiply = 5;
                Timer.IsEnabled = true;
                Timer2.IsEnabled = true;
                label1.Visibility = System.Windows.Visibility.Hidden;
                label2.Visibility = System.Windows.Visibility.Hidden;
                label3.Visibility = System.Windows.Visibility.Hidden;
                label4.Visibility = System.Windows.Visibility.Hidden;
            }

        }
        bool slideMinusSched;
        bool slidePlusSched;
        double selectedWidthSched;
        Label schedLabelWidth;
        Label schedLabelWhite;
        public void Timer3_Tick(object sender, EventArgs e)
        {
            if (slideMinusSched)
            {
                slidePlusSched = false;
                if (gridSchedslide.Width <= selectedWidthSched )
                {
                    
                    if (gridSchedslide.Width == selectedWidthSched)
                    {
                        Timer3.Interval = new TimeSpan(0, 0, 0, 0, 0);
                        this.Timer3.IsEnabled = false;
                        slideMinusSched = false;
                        schedLabelWhite.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        gridSchedslide.Width -= 1;
                    }
                }
                else
                {
                    this.gridSchedslide.Width -= 73.75;
                }

            }
            if (slidePlusSched)
            {
                slideMinusSched = false;
                if (gridSchedslide.Width >= selectedWidthSched)
                {
                    if (this.gridSchedslide.Width == selectedWidthSched)
                    {
                        Timer3.Interval = new TimeSpan(0, 0, 0, 0, 0);
                        this.Timer3.IsEnabled = false;
                        slidePlusSched = false;
                        schedLabelWhite.Visibility = System.Windows.Visibility.Visible;
                    }
                    else
                    {
                        gridSchedslide.Width += 1;
                    }
                }
                else
                {
                    this.gridSchedslide.Width += 73.75;
                }
            }
        }
      
        double gSchedAssign = 1180;
        double gSchedView = 1180 * 2;
        double gSchedStat = 1180 * 3;
        private void label31_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!Timer3.IsEnabled && gridSchedslide.Width != 1180)
            {
                if (gridSchedslide.Width < 1180)
                {
                    slidePlusSched = true;
                }
                else
                {
                    gridSchedslide.Width = 2360;
                    slideMinusSched = true;
                }
                selectedWidthSched = gSchedAssign;
                schedLabelWidth = lAssignSched;
                schedLabelWhite = label34;
                //newlabelWhite = new Label();
                //newlabelWhite = label5;
                Timer3.IsEnabled = true;
                label35.Visibility = System.Windows.Visibility.Hidden;
                label36.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void lViewAssign_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!Timer3.IsEnabled && gridSchedslide.Width != 2360)
            {
                if (gridSchedslide.Width < 2360)
                {
                    slidePlusSched = true;
                }
                else
                {
                    gridSchedslide.Width = 3540;
                    slideMinusSched = true;
                }
                selectedWidthSched = gSchedView;
                schedLabelWidth = lViewAssign;
                schedLabelWhite = label35;
                //newlabel = lCars;
                //newlabelWhite = new Label();
                //newlabelWhite = label5;
                Timer3.IsEnabled = true;
                //Timer.IsEnabled = true;
                label34.Visibility = System.Windows.Visibility.Hidden;
                label36.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void lStat_MouseUp(object sender, MouseButtonEventArgs e)
        {
            if (!Timer3.IsEnabled && gridSchedslide.Width != 3540)
            {
                if (gridSchedslide.Width < 3540)
                {
                    gridSchedslide.Width = 2360;
                    slidePlusSched = true;
                }
                else
                {
                    slideMinusSched = true;
                }
                selectedWidthSched = gSchedStat;
                schedLabelWidth = lStat;
                schedLabelWhite = label36;
                //newlabel = lCars;
                //newlabelWhite = new Label();
                //newlabelWhite = label5;
                Timer3.IsEnabled = true;
                label34.Visibility = System.Windows.Visibility.Hidden;
                label35.Visibility = System.Windows.Visibility.Hidden;
            }
        }
        //Program 
        private void btnAddStud_Click(object sender, RoutedEventArgs e)
        {
            if (tID.Text != "")
            {
                dataStudents.SelectedIndex = -1;
            }
            else
            {
                if (tLname.Text != "" && tFname.Text != "" && tSex.Text != "" && tOcc.Text != "" && tCtzn.Text != "" && tConum.Text != "" && tStat.Text != "" && tAge.Text != "" && tAdrs.Text != "" && tTrans.Text != "" && tHrs.Text != "")
                {
                    string pic;
                    if (iSpp.Source == null) { pic = ""; }
                    else { pic = iSpp.Source.ToString(); }
                    con.Open();
                    cmd = new SqlCommand(@"INSERT INTO STUDENTS
                                  (sDateFill, sLname, sFname, sMI,sBday,sSex,sStat,sCtzn,sOcc,sConum,sAdrs,sPic,pName,pConum,pAdrs,cTrans,cRentHr,sTuition,pRel)
                                  VALUES
                                  ('" + DateTime.Today.ToShortDateString() + "','" + tLname.Text + "','" + tFname.Text + "','" + tMI.Text + "','" + tBday.Text + "','" + tSex.Text + "','" + tStat.Text + "','" + tCtzn.Text + "','" + tOcc.Text + "','" + tConum.Text + "','" + tAdrs.Text + "','" + pic+ "','" + tpName.Text + "','" + tpConum.Text + "','" + tpAdrs.Text + "','" + tTrans.Text + "','" + tHrs.Text + "','" + Convert.ToDouble(tFee.Text).ToString() + "','"+tRel.Text+"')", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential Add Student'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();


                    con.Close();

                    loadDataStudents();
                    loadDataStudentsPay();
                    loadDataStudIns();
                    loadDataStudStatSched();
                    
                   
                    MessageBox.Show("Added Successfully!", "Add", MessageBoxButton.OK, MessageBoxImage.Information);
                    clearTextStudents();

                }
                else
                {
                   MessageBox.Show("Empty Field(s)", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }
        public void loadDataStudents()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT sLname + ', '+ sFname + ' ' + sMI AS sName, sPic , sRegNo, sDateFill, sLname, sFname ,sMI ,sBday ,sSex ,sStat, sCtzn, sOcc, sConum, sAdrs,
                                    pName, pConum, pAdrs,
                                    cTrans, cRentHr,
                                    sTuition,pRel 
                                    FROM STUDENTS 
                                    ORDER BY STUDENTS.sRegNo DESC", con);
            sda = new SqlDataAdapter(cmd);
            dtStudents = new DataTable("STUDENTS");
            sda.Fill(dtStudents);
            dataStudents.ItemsSource = dtStudents.AsDataView();
            con.Close();
        }
        public void loadDataStudentsSrch()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT sLname + ', '+ sFname + ' ' + sMI AS sName, sPic , sRegNo, sDateFill, sLname, sFname ,sMI ,sBday ,sSex ,sStat, sCtzn, sOcc, sConum, sAdrs,
                                    pName, pConum, pAdrs,
                                    cTrans, cRentHr,
                                    sTuition
                                    FROM STUDENTS
                                    WHERE (sLname  LIKE '%"+tSrchStud.Text+"%' OR sFname LIKE '%"+tSrchStud.Text+"%' OR sRegNo LIKE '%"+tSrchStud.Text+"%') ORDER BY STUDENTS.sRegNo DESC", con);
            sda = new SqlDataAdapter(cmd);
            dtStudents = new DataTable("STUDENTS");
            sda.Fill(dtStudents);
            dataStudents.ItemsSource = dtStudents.AsDataView();
            con.Close();
        }
        public void loadDataStudentsPay()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT sLname + ', '+ sFname + ' ' + sMI AS sName, sRegNo, sTuition
                                    FROM STUDENTS 
                                    ORDER BY STUDENTS.sRegNo DESC", con);
            sda = new SqlDataAdapter(cmd);
            dtStudentsPay = new DataTable("STUDENTS");
            sda.Fill(dtStudentsPay);
            dataStudentsPay.ItemsSource = dtStudentsPay.AsDataView();
            con.Close();
            tpayStat.Clear();
            tPartial.Clear();
            tBalance.Clear();
            dataTransactions.ItemsSource = null;
            
        }
        public void loadDataStudentsPaySrch()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT sLname + ', '+ sFname + ' ' + sMI AS sName, sRegNo, sTuition
                                    FROM STUDENTS 
                                    WHERE (sLname  LIKE '%" + tSrchStudPay.Text + "%' OR sFname LIKE '%" + tSrchStudPay.Text + "%' OR sRegNo LIKE '%" + tSrchStudPay.Text + "%') ORDER BY STUDENTS.sRegNo DESC", con);
            sda = new SqlDataAdapter(cmd);
            dtStudentsPay = new DataTable("STUDENTS");
            sda.Fill(dtStudentsPay);
            dataStudentsPay.ItemsSource = dtStudentsPay.AsDataView();
            con.Close();
            tpayStat.Clear();
            tPartial.Clear();
            tBalance.Clear();
            dataTransactions.ItemsSource = null;
        }

        public void loadDataTransactions()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT tID, payDate, payTime, tPay, tPar, tBal, tStatus FROM TRANSACTIONS WHERE sRegNo = '"+lID.Text+"' AND cncled = 'false'", con);
            sda = new SqlDataAdapter(cmd);
            dtTransactions = new DataTable("TRANSACTIONS");
            sda.Fill(dtTransactions);
            dataTransactions.ItemsSource = dtTransactions.AsDataView();
            con.Close();
        }

        public void loadDataTransactionsCanceled()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT tID, payDate, payTime, tPay, tPar, tBal, tStatus, cncleddate FROM TRANSACTIONS WHERE sRegNo = '" + lID.Text + "' AND cncled = 'true'", con);
            sda = new SqlDataAdapter(cmd);
            dtCncledPay = new DataTable("TRANSACTIONS");
            sda.Fill(dtCncledPay);
            dataCncledPay.ItemsSource = dtCncledPay.AsDataView();
            con.Close();
        }


        public void loadDataInstructors()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT iID, iName, iDob, iConum, iAdrs, iIPP FROM INSTRUCTORS", con);
            sda = new SqlDataAdapter(cmd);
            dtInstructors = new DataTable("INSTRUCTORS");
            sda.Fill(dtInstructors);
            dataInstructors.ItemsSource = dtInstructors.AsDataView();
            con.Close();
        }

        public void loadDataCars()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT cID, cName, cPlate, cTrans, cPP FROM CARS", con);
            sda = new SqlDataAdapter(cmd);
            dtCars = new DataTable("CARS");
            sda.Fill(dtCars);
            dataCars.ItemsSource = dtCars.AsDataView();
            con.Close();
        }
        public void loadDataStudIns()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT sLname + ', '+ sFname + ' ' + sMI AS sName, STUDENTS.sRegNo, cTrans FROM STUDENTS
                                   WHERE (sLname  LIKE '%" + tStudentSchedSrch.Text + "%' OR sFname LIKE '%" + tStudentSchedSrch.Text + "%' OR STUDENTS.sRegNo LIKE '%" + tStudentSchedSrch.Text + "%') ORDER BY STUDENTS.sRegNo DESC", con);
            sda = new SqlDataAdapter(cmd);
            dtStudSched = new DataTable("STUDENTS");
            sda.Fill(dtStudSched);
            dataStudSched.ItemsSource = dtStudSched.AsDataView();
            cmd = new SqlCommand(@"SELECT iName, iID FROM INSTRUCTORS", con);
            sda = new SqlDataAdapter(cmd);
            dtInstructorSched = new DataTable("INSTRUCTORS");
            sda.Fill(dtInstructorSched);
            dataInstructorSched.ItemsSource = dtInstructorSched.AsDataView();
            con.Close();
        }
        public void loadDataStudentTotalHrAssigned()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT SUM(DATEPART(HOUR,DATEADD(MINUTE,1,schedTend)) - DATEPART(HOUR,schedTstart)) AS ttlHrAss FROM SCHEDULE_ASSIGN
                                   WHERE sRegNo = '" + tSID .Text + "' AND schedCmplt='false' AND schedNoShow='false'", con);
            rdr = cmd.ExecuteReader();
            while(rdr.Read())
            {
                tHrAssigned.Text = rdr["ttlHrAss"].ToString();
            }
            con.Close();
            if (tHrAssigned.Text == "")
            {
                tHrAssigned.Text = "0";
            }
        }
        public void loadDataCarSched()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT cName, cID FROM CARS WHERE cTrans = '"+tcTransSched.Text+"'", con);
            sda = new SqlDataAdapter(cmd);
            dtCarsSched = new DataTable("CARS");
            sda.Fill(dtCarsSched);
            dataCarSched.ItemsSource = dtCarsSched.AsDataView();
            con.Close();
        }
        public void loadScheduleAssign()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT STUDENTS.sRegNo, sLname +', '+ sFname +' '+ sMI AS sName, schedDate, CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName +'  ('+ CARS.cTrans +')' AS cName  , iName , schedTstart, schedTend, schedTstartMer, schedTendMer, schedID  FROM STUDENTS
                                INNER JOIN SCHEDULE_ASSIGN
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                INNER JOIN INSTRUCTORS
                                ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                INNER JOIN CARS
                                ON CARS.cID = SCHEDULE_ASSIGN.cID
                                WHERE schedCmplt = 'false' AND (schedDate = '" + DateTime.Now.ToShortDateString() + "' AND schedTend >= '" + DateTime.Now.ToString("HH:mm") + "' OR schedDate > '" + DateTime.Now.ToShortDateString() + "') AND (sLname LIKE '%" + tSrchSchedAssign.Text + "%' OR schedDate LIKE '%" + tSrchSchedAssign.Text + "%' OR STUDENTS.sRegNo LIKE '%" + tSrchSchedAssign.Text + "%') AND schedNoShow = 'false' ORDER BY  schedDate, schedTend ASC", con);
            sda = new SqlDataAdapter(cmd);
            dtScheduleAssign = new DataTable("SCHEDULE_ASSIGN");
            sda.Fill(dtScheduleAssign);
            dataScheduleAssign.ItemsSource = dtScheduleAssign.AsDataView();
            con.Close();
        }
        public void loadScheduleAssignOverdue()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT STUDENTS.sRegNo, sLname +', '+ sFname +' '+ sMI AS sName, schedDate, CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName +'  ('+ CARS.cTrans +')' AS cName, iName , schedTstart, schedTend, schedTstartMer, schedTendMer, schedID  FROM STUDENTS
                                INNER JOIN SCHEDULE_ASSIGN
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                INNER JOIN INSTRUCTORS
                                ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                INNER JOIN CARS
                                ON CARS.cID = SCHEDULE_ASSIGN.cID
                                WHERE schedCmplt = 'false' AND (schedDate = '" + DateTime.Now.ToShortDateString() + "' AND schedTend < '" + DateTime.Now.ToString("HH:mm") + "' OR schedDate < '" + DateTime.Now.ToShortDateString() + "') AND (sLname LIKE '%" + tSrchSchedAssign.Text + "%' OR schedDate LIKE '%" + tSrchSchedAssign.Text + "%' OR STUDENTS.sRegNo LIKE '%" + tSrchSchedAssign.Text + "%') AND schedNoShow = 'false' ORDER BY  schedDate, schedTend ASC", con);
            sda = new SqlDataAdapter(cmd);
            dtScheduleAssign = new DataTable("SCHEDULE_ASSIGN");
            sda.Fill(dtScheduleAssign);
            dataScheduleAssign.ItemsSource = dtScheduleAssign.AsDataView();
            con.Close();
        }
        string maxDateFilter;
        string minDateFilter;
        public void GetMaxMinDate()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT MIN(schedDate) AS newMinDate, MAX(schedDate) AS newMaxDate FROM SCHEDULE_ASSIGN
                                    WHERE schedCmplt = 'false' AND schedNoShow = 'false' AND (schedDate = '" + DateTime.Now.ToShortDateString() + "' AND schedTend > '" + DateTime.Now.ToString("HH:mm") + "' OR schedDate > '" + DateTime.Now.ToShortDateString() + "') ", con);
            rdr = cmd.ExecuteReader();
            while(rdr.Read())
            {
                maxDateFilter = rdr["newMinDate"].ToString();
                minDateFilter = rdr["newMaxDate"].ToString();
            }
            con.Close();
            if (maxDateFilter != "" && minDateFilter != "")
            {
                DateTime max = DateTime.Parse(maxDateFilter);
                DateTime min = DateTime.Parse(minDateFilter);
                maxDateFilter = max.ToLongDateString();
                minDateFilter = min.ToLongDateString();
            }
        }
        public void GetMaxMinDateOverdue()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT MIN(schedDate) AS newMinDate, MAX(schedDate) AS newMaxDate FROM SCHEDULE_ASSIGN
                                    WHERE schedCmplt = 'false' AND schedNoShow = 'false' AND (schedDate = '" + DateTime.Now.ToShortDateString() + "' AND schedTend < '" + DateTime.Now.ToString("HH:mm") + "' OR schedDate < '" + DateTime.Now.ToShortDateString() + "') ", con);
            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                maxDateFilter = rdr["newMinDate"].ToString();
                minDateFilter = rdr["newMaxDate"].ToString();
            }
            con.Close();
            if (maxDateFilter != "" && minDateFilter != "")
            {
                DateTime max = DateTime.Parse(maxDateFilter);
                DateTime min = DateTime.Parse(minDateFilter);
                maxDateFilter = max.ToLongDateString();
                minDateFilter = min.ToLongDateString();
            }
        }
        public void loadScheduleAssignDate()
        {
            string dateChnge = Convert.ToDateTime(tSrchAssDate.Text).ToString("MM/dd/yyyy");
            con.Open();
            cmd = new SqlCommand(@"SELECT STUDENTS.sRegNo, sLname +', '+ sFname +' '+ sMI AS sName, schedDate, CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName +'  ('+ CARS.cTrans +')' AS cName  , iName , schedTstart, schedTend, schedTstartMer, schedTendMer, schedID  FROM STUDENTS
                                INNER JOIN SCHEDULE_ASSIGN
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                INNER JOIN INSTRUCTORS
                                ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                INNER JOIN CARS
                                ON CARS.cID = SCHEDULE_ASSIGN.cID
                                WHERE schedCmplt = 'false' AND (schedDate = '" + DateTime.Now.ToShortDateString() + "' AND schedTend >= '" + DateTime.Now.ToString("HH:mm") + "' OR schedDate > '" + DateTime.Now.ToShortDateString() + "') AND (schedDate = '" + dateChnge + "') AND schedNoShow = 'false' ORDER BY  schedDate, schedTend ASC", con);
            sda = new SqlDataAdapter(cmd);
            dtScheduleAssign = new DataTable("SCHEDULE_ASSIGN");
            sda.Fill(dtScheduleAssign);
            dataScheduleAssign.ItemsSource = dtScheduleAssign.AsDataView();
            con.Close();
        }
        public void loadScheduleAssignOverdueDate()
        {
            string dateChnge = Convert.ToDateTime(tSrchAssDate.Text).ToString("MM/dd/yyyy");
            con.Open();
            cmd = new SqlCommand(@"SELECT STUDENTS.sRegNo, sLname +', '+ sFname +' '+ sMI AS sName, schedDate,CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName +'  ('+ CARS.cTrans +')' AS cName, iName , schedTstart, schedTend, schedTstartMer, schedTendMer, schedID  FROM STUDENTS
                                INNER JOIN SCHEDULE_ASSIGN
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                INNER JOIN INSTRUCTORS
                                ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                INNER JOIN CARS
                                ON CARS.cID = SCHEDULE_ASSIGN.cID
                                WHERE schedCmplt = 'false' AND (schedDate = '" + DateTime.Now.ToShortDateString() + "' AND schedTend < '" + DateTime.Now.ToString("HH:mm") + "' OR schedDate < '" + DateTime.Now.ToShortDateString() + "') AND (schedDate = '" + dateChnge + "') AND  schedNoShow = 'false' ORDER BY  schedDate, schedTend ASC", con);
            sda = new SqlDataAdapter(cmd);
            dtScheduleAssign = new DataTable("SCHEDULE_ASSIGN");
            sda.Fill(dtScheduleAssign);
            dataScheduleAssign.ItemsSource = dtScheduleAssign.AsDataView();
            con.Close();
        }
        public void GetTimeLeftSched()
        {
            string tleft = "";
            string cRentHr = "";
            con.Open();
            cmd = new SqlCommand(@"SELECT cRentHr - SUM(DATEPART(HOUR,DATEADD(MINUTE,1,schedTend)) - DATEPART(HOUR,schedTstart))AS ttlLeft, cRentHr FROM SCHEDULE_ASSIGN
                                INNER JOIN STUDENTS
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                WHERE STUDENTS.sRegNo = '" +tSID.Text+"' AND schedCmplt='true' AND schedNoShow='false' GROUP BY cRentHr;", con);
            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            { 
                tleft = rdr["ttlLeft"].ToString();
            }
            con.Close();
            con.Open();
            cmd = new SqlCommand(@"SELECT cRentHr FROM STUDENTS WHERE sRegNo = '" + tSID.Text + "';", con);
            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                cRentHr = rdr["cRentHr"].ToString();
            }
            con.Close();
            if (tleft == "")
            {
                tHrLeft.Text = cRentHr;
            }
            else
            {
                tHrLeft.Text = tleft;
            }
            
        }
        public void loadDataStudStatSched()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT sLname + ', '+ sFname + ' ' + sMI AS sName, STUDENTS.sRegNo, cRentHr FROM STUDENTS
                                   WHERE (sLname  LIKE '%" + tSrchStudSched.Text + "%' OR sFname LIKE '%" + tSrchStudSched.Text + "%' OR STUDENTS.sRegNo LIKE '%" + tSrchStudSched.Text + "%') ORDER BY STUDENTS.sRegNo DESC", con);
            sda = new SqlDataAdapter(cmd);
            dtStudSchedStat = new DataTable("STUDENTS");
            sda.Fill(dtStudSchedStat);
            dataStudentsStatus.ItemsSource = dtStudSchedStat.AsDataView();
            con.Close();
            tStatStat.Clear();
            tStatTimeLeft.Clear();
            tTimeUsedStat.Clear();
        }
        public void GetTotalHoursAssignedComplete()
        {
            string ttlHrcmplt = "";
            con.Open();
            cmd = new SqlCommand(@"SELECT SUM(DATEDIFF(HOUR,schedTstart,DATEADD(MINUTE,1,schedTend))) AS ttlHrCmplt FROM SCHEDULE_ASSIGN
                                   WHERE sRegNo = '" + tStudStatID .Text+ "' AND schedCmplt='true' AND schedNoShow='false'", con);
            rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                
               ttlHrcmplt = rdr["ttlHrCmplt"].ToString();
            }
            con.Close();
            if (ttlHrcmplt == "")
            {
                tTimeUsedStat.Text = "0";
            }
            else
            {
                tTimeUsedStat.Text = ttlHrcmplt;
            }
        }
        public void loadDataSchedStatCmplt()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT STUDENTS.sRegNo, schedDate, CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName +'  ('+ CARS.cTrans +')' AS cName  , iName ,schedID ,DATEDIFF(HOUR,schedTstart,DATEADD(MINUTE,1,schedTend)) AS Hrlen, schedStatus FROM STUDENTS
                                INNER JOIN SCHEDULE_ASSIGN
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                INNER JOIN INSTRUCTORS
                                ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                INNER JOIN CARS
                                ON CARS.cID = SCHEDULE_ASSIGN.cID
                                WHERE schedCmplt = 'true' AND schedNoShow = 'false' AND SCHEDULE_ASSIGN.sRegNo = '" + tStudStatID.Text + "'  ORDER BY  schedDate, schedTend ASC", con);
            sda = new SqlDataAdapter(cmd);
            dtScheduleComplt = new DataTable("SCHEDULE_ASSIGN");
            sda.Fill(dtScheduleComplt);
            dataSchedStatComplt.ItemsSource = dtScheduleComplt.AsDataView();
            con.Close();
        }
        public void loadDataSchedAbsences()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT sLname + ', '+ sFname + ' ' + sMI AS sName, STUDENTS.sRegNo, schedDate, CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName +'  ('+ CARS.cTrans +')' AS cName  , iName ,schedID ,DATEDIFF(HOUR,schedTstart,schedTend) AS Hrlen FROM STUDENTS
                                INNER JOIN SCHEDULE_ASSIGN
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                INNER JOIN INSTRUCTORS
                                ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                INNER JOIN CARS
                                ON CARS.cID = SCHEDULE_ASSIGN.cID
                                WHERE schedCmplt = 'false' AND schedNoShow = 'true' AND SCHEDULE_ASSIGN.sRegNo = '" + tStudStatID.Text + "'  ORDER BY  schedDate, schedTend ASC", con);
            sda = new SqlDataAdapter(cmd);
            dtScheduleAbsent = new DataTable("SCHEDULE_ASSIGN");
            sda.Fill(dtScheduleAbsent);
            dataScheduleAbsent.ItemsSource = dtScheduleAbsent.AsDataView();
            con.Close();
        }
        public void loadSchedTodayHome()
        {
            con.Open();
            cmd = new SqlCommand(@"SELECT STUDENTS.sRegNo, sLname +', '+ sFname +' '+ sMI AS sName, schedDate, CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName +'  ('+ CARS.cTrans +')' AS cName  , iName , schedTstart, schedTend, schedTstartMer, schedTendMer, schedID  FROM STUDENTS
                                INNER JOIN SCHEDULE_ASSIGN
                                ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                INNER JOIN INSTRUCTORS
                                ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                INNER JOIN CARS
                                ON CARS.cID = SCHEDULE_ASSIGN.cID
                                WHERE schedCmplt = 'false' AND (schedDate = '" + DateTime.Now.ToShortDateString() + "') AND schedNoShow = 'false' ORDER BY  schedDate, schedTend ASC", con);
            sda = new SqlDataAdapter(cmd);
            dtHome = new DataTable("SCHEDULE_ASSIGN");
            sda.Fill(dtHome);
            dataHome.ItemsSource = dtHome.AsDataView();
            con.Close();
        }
        public void clearTextStudents()
        {
            tID.Clear();
            tBday.Clear();
            textBox1.Clear();
            tLname.Clear();
            tFname.Clear();
            tMI.Clear();
            tAge.Clear();
            tOcc.Clear();
            tStat.SelectedIndex = -1;
            tCtzn.Clear();
            tAdrs.Clear();
            tpAdrs.Clear();
            tpName.Clear();
            tpConum.Clear();
            tTrans.SelectedIndex = -1;
            tHrs.SelectedIndex = -1;
            cbMth.SelectedIndex = 0;
            cbDay.SelectedIndex = 0;
            cbYear.SelectedIndex = 0;
            tDateFill.Text = DateTime.Now.ToShortDateString();
            iSpp.Source = null;
            tRel.Clear();

        }
        public void cleartextSchedAssign()
        {

            loadDataStudIns();
            dataCarSched.ItemsSource = null;
            cbStart.SelectedIndex = -1;
            cbStartMer.SelectedIndex = -1;
            cbEnd.SelectedIndex = -1;
            cbEndMer.SelectedIndex = -1;
            dtSchedDate.SelectedDate = null;
            dtSchedDate.Text = "";
            tHrLeft.Clear();
        }
        public void clearTextInstructors()
        {
            iID.Clear();
            iName.Clear();
            iAdrs.Clear();
            iConum.Clear();
            iBday.Clear();
            iMnth.SelectedIndex = 0;
            iDay.SelectedIndex = 0;
            iYr.SelectedIndex = 0;
            iIpp.Source = null;

        }

        public void clearTextCars()
        {
            cID.Clear();
            cName.Clear();
            cPlate.Clear();
            cTrans.SelectedIndex = -1;
            iCpp.Source = null;

        }

        private void btnSImage_Click(object sender, RoutedEventArgs e)
        {
            
        }

        private void tLname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tLname.Text == "")
            {
                lLname.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                lLname.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tFname_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tFname.Text == "")
            {
                lFname.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                lFname.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void tMI_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tMI.Text == "")
            {
                lMI.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                lMI.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        #region BirthDay;
        string sDOB;
        string bdMth = "";
        string bdDay = "";
        string bdYear = "";

        string iDOB;
        string bdiMth = "";
        string bdiDay = "";
        string bdiYear = "";
        private void cbMth_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbMth.SelectedIndex < 10)
            {
                bdMth = "0" + cbMth.SelectedIndex.ToString();
            }
            else
            {
                bdMth = cbMth.SelectedIndex.ToString();
            }
            sDOB = bdMth + "/" + bdDay + "/" + bdYear;
            DateTime result;
            bool r = DateTime.TryParse(sDOB, out result);
            if(r)
            {
                this.tBday.Text = sDOB;
            }
            else
            {
                this.tBday.Clear();
            }
        }

        private void cbDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbDay.SelectedIndex < 10)
            {
                bdDay = "0" + cbDay.SelectedIndex.ToString();
            }
            else
            {
                bdDay = cbDay.SelectedIndex.ToString();
            }
            sDOB = bdMth + "/" + bdDay + "/" + bdYear;
            DateTime result;
            bool r = DateTime.TryParse(sDOB, out result);
            if (r)
            {
                this.tBday.Text = sDOB;
            }
            else
            {
                this.tBday.Clear();
            }
        }

        private void cbYear_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bdYear = cbYear.SelectedItem.ToString();
            sDOB = bdMth + "/" + bdDay + "/" + bdYear;
            DateTime result;
            bool r = DateTime.TryParse(sDOB, out result);
            if (r)
            {
                this.tBday.Text = sDOB;
            }
            else
            {
                this.tBday.Clear();
            }
        }

        private void tBday_TextChanged(object sender, TextChangedEventArgs e)
        {
            DateTime resulttBday;
            bool r = DateTime.TryParse(tBday.Text, out resulttBday);
            int dtAge = DateTime.Now.Year - resulttBday.Year;
            if (resulttBday.Month <= DateTime.Now.Month && resulttBday.Day <= DateTime.Now.Day)
            {
                dtAge = dtAge + 1;
            }
            tAge.Text = dtAge.ToString();
            if (tBday.Text == "")
            {
                tAge.Clear();
            }
        }
        private void iMnth_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (iMnth.SelectedIndex < 10)
            {
                bdiMth = "0" + iMnth.SelectedIndex.ToString();
            }
            else
            {
                bdiMth = iMnth.SelectedIndex.ToString();
            }
            iDOB = bdiMth + "/" + bdiDay + "/" + bdiYear;
            DateTime result;
            bool r = DateTime.TryParse(iDOB, out result);
            if (r)
            {
                this.iBday.Text = iDOB;
            }
            else
            {
                this.iBday.Clear();
            }
        }

        private void iDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (iDay.SelectedIndex < 10)
            {
                bdiDay = "0" + iDay.SelectedIndex.ToString();
            }
            else
            {
                bdiDay = iDay.SelectedIndex.ToString();
            }
            iDOB = bdiMth + "/" + bdiDay + "/" + bdiYear;
            DateTime result;
            bool r = DateTime.TryParse(iDOB, out result);
            if (r)
            {
                this.iBday.Text = iDOB;
            }
            else
            {
                this.iBday.Clear();
            }
        }

        private void iYr_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            bdiYear = iYr.SelectedItem.ToString();
            iDOB = bdiMth + "/" + bdiDay + "/" + bdiYear;
            DateTime result;
            bool r = DateTime.TryParse(iDOB, out result);
            if (r)
            {
                this.iBday.Text = iDOB;
            }
            else
            {
                this.iBday.Clear();
            }
        }

        private void iBday_TextChanged(object sender, TextChangedEventArgs e)
        {
            DateTime resulttBday;
            bool r = DateTime.TryParse(iBday.Text, out resulttBday);
            int dtAge = DateTime.Now.Year - resulttBday.Year;
            if (resulttBday.Month <= DateTime.Now.Month && resulttBday.Day <= DateTime.Now.Day)
            {
                dtAge = dtAge + 1;
            }
            iAge.Text = dtAge.ToString();
            if (iBday.Text == "")
            {
                iAge.Clear();
            }
        }
        #endregion

        #region Pricing
        private void tTrans_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tTrans.SelectedIndex == 0 && tHrs.SelectedIndex == 0)
            {
                tFee.Text = "2,000";
            }
            if (tTrans.SelectedIndex == 0 && tHrs.SelectedIndex == 1)
            {
                tFee.Text = "3,800";
            }
            if (tTrans.SelectedIndex == 0 && tHrs.SelectedIndex == 2)
            {
                tFee.Text = "5,700";
            }
            if (tTrans.SelectedIndex == 1 && tHrs.SelectedIndex == 0)
            {
                tFee.Text = "3,000";
            }
            if (tTrans.SelectedIndex == 1 && tHrs.SelectedIndex == 1)
            {
                tFee.Text = "5,000";
            }
            if (tTrans.SelectedIndex == 1 && tHrs.SelectedIndex == 2)
            {
                tFee.Text = "7,000";
            }
        }

        private void tHrs_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (tTrans.SelectedIndex == 0 && tHrs.SelectedIndex == 0)
            {
                tFee.Text = "2,000";
            }
            if (tTrans.SelectedIndex == 0 && tHrs.SelectedIndex == 1)
            {
                tFee.Text = "3,800";
            }
            if (tTrans.SelectedIndex == 0 && tHrs.SelectedIndex == 2)
            {
                tFee.Text = "5,700";
            }
            if (tTrans.SelectedIndex == 1 && tHrs.SelectedIndex == 0)
            {
                tFee.Text = "3,000";
            }
            if (tTrans.SelectedIndex == 1 && tHrs.SelectedIndex == 1)
            {
                tFee.Text = "5,000";
            }
            if (tTrans.SelectedIndex == 1 && tHrs.SelectedIndex == 2)
            {
                tFee.Text = "7,000";
            }
        }
        #endregion

        string transClone, hrsClone, feeClone;
        private void dataStudents_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {
            if (tBday.Text != "")
            {
                int mth = Convert.ToDateTime(tBday.Text).Month;
                int day = Convert.ToDateTime(tBday.Text).Day;
                string yr = Convert.ToDateTime(tBday.Text).Year.ToString();
                cbMth.SelectedIndex = mth;
                cbDay.SelectedIndex = day;
                cbYear.SelectedValue = yr;
            }
            else
            {
                clearTextStudents();
            }
            transClone = tTrans.Text;
            hrsClone = tHrs.Text;
            feeClone = tFee.Text;
        }

        private void btnUpdateStud_Click(object sender, RoutedEventArgs e)
        {
            if (tID.Text != "")
            {
                if (tLname.Text != "" && tFname.Text != ""  && tSex.Text != "" && tOcc.Text != "" && tCtzn.Text != "" && tConum.Text != "" && tStat.Text != "" && tAge.Text != "" && tAdrs.Text != "" && tTrans.Text != "" && tHrs.Text != "")
                {
                    string pic;
                    
                    if (iSpp.Source == null) { pic = ""; }
                    else { pic = iSpp.Source.ToString(); }

                    if (tTrans.Text != transClone || tHrs.Text != hrsClone)
                    {
                        MessageBoxResult mi = MessageBox.Show("Changing the tuition fee will reset the student's payments, Do you still want to continue?", "", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                        if (mi == MessageBoxResult.Yes)
                        {
                            con.Open();

                            cmd = new SqlCommand(@"UPDATE STUDENTS
                                SET
                                sLname = '" + tLname.Text + "',sFname = '" + tFname.Text + "',sMI = '" + tMI.Text + "',sBday = '" + tBday.Text + "',sSex = '" + tSex.Text + "',sStat = '" + tStat.Text + "',sCtzn = '" + tCtzn.Text + "',sOcc = '" + tOcc.Text + "',sConum = '" + tConum.Text + "',sAdrs = '" + tAdrs.Text + "',sPic = '" + pic + "', pName = '" + tpName.Text + "', pConum = '" + tpConum.Text + "', pAdrs = '" + tpAdrs.Text + "', cTrans = '" + tTrans.Text + "', cRentHr = '" + tHrs.Text + "', sTuition = '" + Convert.ToDouble(tFee.Text).ToString() + "'   WHERE sRegNo = '" + tID.Text + "';", con);
                            cmd.ExecuteNonQuery();
                            cmd = new SqlCommand(@"DELETE FROM TRANSACTIONS WHERE sRegNo='"+tID.Text+"'",con);
                            cmd.ExecuteNonQuery();

                            //string backupname = @"NAME = N'HVNds-Differential Update Student'";
                            //cmd = new SqlCommand(differential + backupname, con);
                            //cmd.ExecuteNonQuery();

                            con.Close();
                            loadDataStudents();
                            loadDataStudentsPay();
                            loadDataStudIns();

                            MessageBox.Show("Updated!", "Update", MessageBoxButton.OK, MessageBoxImage.Information);
                            clearTextStudents();
                        }
                        else
                        {
                            tTrans.Text = transClone;
                            tHrs.Text = hrsClone;
                            tFee.Text = feeClone;
                        }
                    }
                    else
                    {
                        con.Open();

                        cmd = new SqlCommand(@"UPDATE STUDENTS
                                SET
                                sLname = '" + tLname.Text + "',sFname = '" + tFname.Text + "',sMI = '" + tMI.Text + "',sBday = '" + tBday.Text + "',sSex = '" + tSex.Text + "',sStat = '" + tStat.Text + "',sCtzn = '" + tCtzn.Text + "',sOcc = '" + tOcc.Text + "',sConum = '" + tConum.Text + "',sAdrs = '" + tAdrs.Text + "',sPic = '" + pic + "', pName = '" + tpName.Text + "', pConum = '" + tpConum.Text + "', pAdrs = '" + tpAdrs.Text + "'   WHERE sRegNo = '" + tID.Text + "';", con);
                        cmd.ExecuteNonQuery();

                        //string backupname = @"NAME = N'HVNds-Differential Update Student'";
                        //cmd = new SqlCommand(differential + backupname, con);
                        //cmd.ExecuteNonQuery();

                        con.Close();
                        loadDataStudents();
                        loadDataStudentsPay();
                        loadDataStudIns();

                        MessageBox.Show("Updated!", "Update", MessageBoxButton.OK, MessageBoxImage.Information);
                        clearTextStudents();
                    }
                }
                else
                {
                    MessageBox.Show("Empty Fields!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Select Student First!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void textBox1_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (textBox1.Text != "")
            {
                string ppss = textBox1.Text;
                int ppslen = ppss.Length;
                string pppath = ppss.Substring(8, ppslen - 8);

                if (File.Exists(pppath))
                {
                    BitmapImage logo = new BitmapImage();
                    logo.BeginInit();
                    //logo.UriSource = new Uri("pack://application:,,,/MainProjectNew;component/Images/Xbtn.png");
                    logo.UriSource = new Uri(textBox1.Text);
                    logo.EndInit();
                    iSpp.Source = logo;
                }
                else 
                {
                    MessageBox.Show("Profile Picture not found.", "Error");
                }
            }
            else
            {
                iSpp.Source = null;
            }

            
        }

        private void tSrchStud_TextChanged(object sender, TextChangedEventArgs e)
        {
            loadDataStudentsSrch();
            if (tSrchStud.Text != "")
            {
                lSrchStud.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                lSrchStud.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void tSrchStudPay_TextChanged(object sender, TextChangedEventArgs e)
        {
            loadDataStudentsPaySrch();
            if (tSrchStudPay.Text != "")
            {
                lStudPaySrch.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                lStudPaySrch.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void btnPay_Click(object sender, RoutedEventArgs e)
        {
            if (lID.Text != "")
            {
                Dim.Visibility = System.Windows.Visibility.Visible;
                gridPay.Visibility = System.Windows.Visibility.Visible;
                gridPayCncled.Visibility = System.Windows.Visibility.Hidden;
                gridConflict.Visibility = System.Windows.Visibility.Hidden;
                gridStatAbsences.Visibility = System.Windows.Visibility.Hidden;
                gridCam.Visibility = System.Windows.Visibility.Hidden;
                tPay.Focus();
                tPay.Clear();
            }
            else
            {
                MessageBox.Show("Select from Student first!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCloseDialog_Click(object sender, RoutedEventArgs e)
        {
            Dim.Visibility = System.Windows.Visibility.Collapsed;
            tNotifyPay.Clear();
        }

        private void tPay_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tPay.Text != "")
            {
                lPayAmount.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                lPayAmount.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void btnPayFee_Click(object sender, RoutedEventArgs e)
        {
            if (tPay.Text != "")
            {
                string paystatus = "";
                float partial = float.Parse(tPartial.Text);
                float payment = float.Parse(tPay.Text);
                float tuition = float.Parse(lFee.Text);
                float newpartial = partial + payment;
                float balance = tuition - newpartial;

                if (balance == 0)
                {
                    paystatus = "Fully paid";
                }
                else
                {
                    paystatus = "Not fully paid";
                }
                if (tuition < payment || balance < 0)
                {
                    System.Media.SystemSounds.Hand.Play();
                    tNotifyPay.Text = "Payment is greater than tuition";
                    tNotifyPay.Foreground= new SolidColorBrush(Colors.Red);
                }
                else
                {
                    int selected = dataStudentsPay.SelectedIndex;
                    con.Open();
                    cmd = new SqlCommand(@"INSERT INTO TRANSACTIONS
                                          (sRegNo,payDate,payTime,tPay,tPar,tBal,tStatus,cncled)
                                          VALUES
                                          ('" + lID.Text + "','" + DateTime.Now.ToShortDateString() + "','" + DateTime.Now.ToShortTimeString()+ "','" + payment.ToString() + "','" + newpartial.ToString() + "','" + balance.ToString() + "','" + paystatus + "','false')", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential AS New Payment'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    System.Media.SystemSounds.Beep.Play();
                    loadDataStudentsPay();
                    dataStudentsPay.SelectedIndex = selected;
                    tPay.Clear();
                    tNotifyPay.Text = "Paid!";
                    tNotifyPay.Foreground = new SolidColorBrush(Colors.Lime);
                }
            }
            else
            {
                System.Media.SystemSounds.Hand.Play();
                tNotifyPay.Text = "Enter Amount First";
                tNotifyPay.Foreground = new SolidColorBrush(Colors.Red);
            }
        }

        private void tPartial_TextChanged(object sender, TextChangedEventArgs e)
        {
            
        }
        private void dataStudentsPay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

            if (lID.Text != "")
            {
                loadDataTransactions();
            }
            if (dataTransactions.Items.Count == 0)
            {
                tPartial.Text = "0.0";
                tBalance.Text = lFee.Text;
                tpayStat.Text = "Not fully paid";
                btnCancelPay.IsEnabled = false;
            }
            else
            {
                dataTransactions.SelectedIndex = dataTransactions.Items.Count - 1;
                btnCancelPay.IsEnabled = true;
            }
            if (tBalance.Text == "0.00")
            {
                btnPay.IsEnabled = false;
            }
            else
            {
                btnPay.IsEnabled = true;
            }
            
            if (dataTransactions.Items.Count <= 10)
            {
                dataTransactions.Background = new SolidColorBrush(Colors.Transparent);
            }
            else
            {
                dataTransactions.Background = new SolidColorBrush(Colors.White);
            }
            
        }

        private void btnCancelPay_Click(object sender, RoutedEventArgs e)
        {
            if (lID.Text != "")
            {
                if (MessageBox.Show("Cancel latest payment?", "Cancel?", MessageBoxButton.YesNo, MessageBoxImage.Question) == MessageBoxResult.Yes)
                {
                    int selected = dataStudentsPay.SelectedIndex;
                    con.Open();
                    cmd = new SqlCommand(@"SELECT COUNT(*) AS counts FROM TRANSACTIONS WHERE cncled = 'true' AND sRegNo = '" + lID.Text + "'", con);
                    rdr = cmd.ExecuteReader();
                    int cnt = 0;
                    while (rdr.Read())
                    {
                        cnt = int.Parse(rdr["counts"].ToString());
                    }
                    con.Close();
                    if (cnt > 19)
                    {
                        con.Open();
                        cmd = new SqlCommand(@"DELETE FROM TRANSACTIONS WHERE sRegNo = '" + lID.Text + "' AND cncled = 'true'", con);
                        cmd.ExecuteNonQuery();
                        con.Close();
                        cnt = 0;
                    }
                    con.Open();
                    cmd = new SqlCommand(@"UPDATE TRANSACTIONS SET cncled = 'true', cncleddate = '" + DateTime.Now.ToString() + "', cnclnum = '" + (cnt++) + "' WHERE tID = '" + tPayID.Text + "';", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential Cancel Payment'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    loadDataStudentsPay();
                    MessageBox.Show("Cancelled!", "Cancel", MessageBoxButton.OK, MessageBoxImage.Information);
                    dataStudentsPay.SelectedIndex = selected;
                }

            }
            else
            {
                MessageBox.Show("Select from Student first!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancelledPay_Click(object sender, RoutedEventArgs e)
        {
            Dim.Visibility = System.Windows.Visibility.Visible;
            gridPay.Visibility = System.Windows.Visibility.Visible;
            gridPayCncled.Visibility = System.Windows.Visibility.Visible;
            gridConflict.Visibility = System.Windows.Visibility.Hidden;
            gridStatAbsences.Visibility = System.Windows.Visibility.Hidden;
            gridCam.Visibility = System.Windows.Visibility.Hidden;
            loadDataTransactionsCanceled();
            
        }

        private void dataTransactions_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            dataTransactions.SelectedIndex = dataTransactions.Items.Count - 1; 
        }

        private void btnAddIn_Click(object sender, RoutedEventArgs e)
        {
            if (iID.Text != "")
            {
                dataInstructors.SelectedIndex = -1;
            }
            else
            {
                if (iName.Text != "" && iAdrs.Text != "" && iBday.Text != "" && iConum.Text != "")
                {
                    string pic;
                    if (iIpp.Source == null) { pic = ""; }
                    else { pic = iIpp.Source.ToString(); }
                    con.Open();
                    cmd = new SqlCommand(@"INSERT INTO INSTRUCTORS
                                (iName,iDob,iConum,iAdrs,iIPP)
                                VALUES
                                ('" + iName.Text + "','" + iBday.Text + "','" + iConum.Text + "','" + iAdrs.Text + "','" +pic + "')", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential Add Instructor'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    loadDataInstructors();
                    loadDataStudIns();
                    cleartextSchedAssign();
                    MessageBox.Show("Added Successfully!", "Added", MessageBoxButton.OK, MessageBoxImage.Information);
                    clearTextInstructors();
                }
                else
                {
                    MessageBox.Show("Empty Field(s)", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            
        }

        private void tIpicloc_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (tIpicloc.Text != "")
            {
                BitmapImage logo = new BitmapImage();
                logo.BeginInit();
                logo.UriSource = new Uri(tIpicloc.Text);
                logo.EndInit();
                iIpp.Source = logo;
            }
            else
            {
                iIpp.Source = null;
            }
        }

        private void btnUpIn_Click(object sender, RoutedEventArgs e)
        {
            if (iID.Text != "")
            {
                if (iName.Text != "" && iAdrs.Text != "" && iBday.Text != "" && iConum.Text != "")
                {
                    string pic;
                    if (iIpp.Source == null) { pic = ""; }
                    else { pic = iIpp.Source.ToString(); }
                    con.Open();
                    cmd = new SqlCommand(@"UPDATE INSTRUCTORS
                                SET
                                iName = '" + iName.Text + "', iAdrs = '" + iAdrs.Text + "', iDob = '" + iBday.Text + "', iConum = '" + iConum.Text + "', iIPP = '" + pic + "' WHERE iID = '" + iID.Text + "'", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential Update Instructor'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    loadDataInstructors();
                    loadDataStudIns();
                    cleartextSchedAssign();
                    MessageBox.Show("Updated!", "Update", MessageBoxButton.OK, MessageBoxImage.Information);
                    clearTextInstructors();
                }
                else
                {
                    MessageBox.Show("Empty Fields!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Select Instructor First!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dataInstructors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (iBday.Text != "")
            {
                int mth = Convert.ToDateTime(iBday.Text).Month;
                int day = Convert.ToDateTime(iBday.Text).Day;
                string yr = Convert.ToDateTime(iBday.Text).Year.ToString();
                iMnth.SelectedIndex = mth;
                iDay.SelectedIndex = day;
                iYr.SelectedValue = yr;
            }
            else
            {
                clearTextInstructors();
            }
        }

        private void btnAddCar_Click(object sender, RoutedEventArgs e)
        {
            if (cID.Text != "")
            {
                dataCars.SelectedIndex = -1;
            }
            else
            {
                if (cName.Text != "" && cPlate.Text != "" && cTrans.Text != "")
                {
                    string pic;
                    if (iCpp.Source == null){pic = "";}
                    else{pic = iCpp.Source.ToString();}
                    con.Open();
                    cmd = new SqlCommand(@"INSERT INTO CARS
                                (cName,cTrans,cPlate,cPP)
                                VALUES
                                ('"+cName.Text+"','"+cTrans.Text+"','"+cPlate.Text+"','"+pic+"')", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential Add Car'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    loadDataCars();
                    loadDataStudIns();
                    cleartextSchedAssign();
                    MessageBox.Show("Added Successfully!", "Added", MessageBoxButton.OK, MessageBoxImage.Information);
                    clearTextCars();
                }
                else
                {
                    MessageBox.Show("Empty Field(s)", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        private void btnCpic_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openfd = new Microsoft.Win32.OpenFileDialog();
            openfd.Filter = "Images |* .jpg; *.jepeg; *.png; *.gif;";
            openfd.ShowDialog();
            if (openfd.FileName.Count() != 0)
            {
                iCpp.Source = new BitmapImage(new Uri(openfd.FileName));

            }
        }

        private void cIpploc_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (cIpploc.Text != "")
            {
                BitmapImage logo = new BitmapImage();
                logo.BeginInit();
                logo.UriSource = new Uri(cIpploc.Text);
                logo.EndInit();
                iCpp.Source = logo;
            }
            else
            {
                iCpp.Source = null;
            }
        }

        private void btnUpCar_Click(object sender, RoutedEventArgs e)
        {
            if (cID.Text != "")
            {
                if (cName.Text != "" && cPlate.Text != "" && cTrans.Text != "")
                {
                    string pic;
                    if (iCpp.Source == null) { pic = ""; }
                    else { pic = iCpp.Source.ToString(); }
                    con.Open();
                    cmd = new SqlCommand(@"UPDATE CARS
                                SET
                                cName = '"+cName.Text+"', cTrans = '"+cTrans.Text+"', cPlate = '"+cPlate.Text+"', cPP = '"+pic+"' WHERE cID = '"+cID.Text+"'", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential Update Car'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    loadDataCars();
                    loadDataStudIns();
                    cleartextSchedAssign();
                    MessageBox.Show("Updated!", "Update", MessageBoxButton.OK, MessageBoxImage.Information);
                    clearTextCars();
                }
                else
                {
                    MessageBox.Show("Empty Fields!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Select Car First!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void dataStudSched_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if(tcTransSched.Text != "")
            {
                loadDataCarSched();
            }
            if (dataStudSched.SelectedIndex != -1)
            {
                loadDataStudentTotalHrAssigned();
                GetTimeLeftSched();
            }
        }
        bool rerun = false;
        private void btnAssignSched_Click(object sender, RoutedEventArgs e)
        {
            if (dtSchedDate.Text != "" && cbStart.SelectedIndex != -1 && cbStartMer.SelectedIndex != -1 && cbEnd.SelectedIndex != -1 && dataStudSched.SelectedIndex != -1 && dataCarSched.SelectedIndex != -1 && dataInstructorSched.SelectedIndex != -1)
            {   
                String sDate = DateTime.Parse(dtSchedDate.Text).ToShortDateString();
                String sStart = DateTime.Parse(cbStart.Text + " " + cbStartMer.Text).ToString("HH:mm");
                DateTime tEnd = DateTime.Parse(cbEnd.Text + " " + cbEndMer.Text).AddMinutes(-1);
                String sEnd = tEnd.ToString("HH:mm");

                int tlength = int.Parse(sEnd.Substring(0,2))-int.Parse(sStart.Substring(0,2));
                int tLeft = int.Parse(tHrLeft.Text);
                int tAssigned = int.Parse(tHrAssigned.Text) + tlength+1;
                if (DateTime.Parse(sEnd) > DateTime.Parse(sStart))
                {
                    if (tLeft >= tAssigned)
                    {
                        
                        if (DateTime.Parse(dtSchedDate.Text) < DateTime.Now && Convert.ToDateTime(sEnd.ToString() + ":00") < DateTime.Now && rerun == false)
                        {
                            MessageBoxResult mi = MessageBox.Show("Assigned time already ended!", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                            if (mi == MessageBoxResult.Yes)
                            {
                                rerun = true;
                                btnAssignSched.RaiseEvent(new RoutedEventArgs(System.Windows.Controls.Primitives.ButtonBase.ClickEvent));
                            }
                            else
                            {
                                rerun = false;
                            }
                        }
                        else
                        {
                            rerun = true;
                        }
                        if (rerun)
                        {
                            con.Open();
                            cmd = new SqlCommand(@"SELECT sLname +', '+ sFname +' '+ sMI AS sName, schedDate, CAST(CASE WHEN LEN(CONVERT(varchar(15),schedTstart,100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),schedTstart,100),0,LEN(CONVERT(varchar(15),schedTstart,100))-1)+' '+schedTstartMer END AS varchar(50)) +' - '+ CAST(CASE WHEN LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100)) > 6 THEN SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer ELSE '0'+SUBSTRING(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100),0,LEN(CONVERT(varchar(15),DATEADD(MINUTE,1,schedTend),100))-1)+' '+schedTendMer END AS varchar(50)) AS schedTime, cName , iName ,schedTstart, schedTend, schedTstartMer, schedTendMer, schedID  FROM STUDENTS
                                                    
                                                        INNER JOIN SCHEDULE_ASSIGN
                                                        ON STUDENTS.sRegNo = SCHEDULE_ASSIGN.sRegNo
                                                        INNER JOIN INSTRUCTORS
                                                        ON INSTRUCTORS.iID = SCHEDULE_ASSIGN.iID
                                                        INNER JOIN CARS
                                                        ON CARS.cID = SCHEDULE_ASSIGN.cID
                                                        WHERE schedDate = '" + sDate + "'AND (STUDENTS.sRegNo = '" + tSID.Text + "' OR INSTRUCTORS.iID = '" + tiID.Text + "' OR  CARS.cID = '" + tcID.Text +
                                                                    "') AND (schedCmplt = 'false') AND (schedTstart BETWEEN '" + sStart + "' AND '" + sEnd +
                                                                    "' OR schedTend BETWEEN '" + sStart + "' AND '" + sEnd +
                                                                    "' AND schedTstart > '" + sStart + "' AND schedTstart < '" + sEnd +
                                                                    "' OR schedTend > '" + sStart + "' AND schedTend < '" + sEnd +
                                                                    "'OR '" + sStart + "' BETWEEN schedTstart AND schedTend AND '" + sEnd + "' BETWEEN schedTstart AND schedTend) AND schedNoShow = 'false'", con);
                            rdr = cmd.ExecuteReader();
                            bool read = false;
                            while (rdr.Read())
                            {
                                read = true;
                            }
                            rdr.Close();
                            sda = new SqlDataAdapter(cmd);
                            dtSchedConflict = new DataTable("SCHEDULE_ASSIGN");
                            sda.Fill(dtSchedConflict);
                            dataSchedConflict.ItemsSource = dtSchedConflict.AsDataView();
                            con.Close();
                            if (!read)
                            {
                                con.Open();
                                cmd = new SqlCommand(@"INSERT INTO SCHEDULE_ASSIGN
                                    (sRegNo,cID,iID,schedDate,schedTstart,schedTend,schedTstartMer,schedTendMer,schedCmplt,schedNoShow )
                                    VALUES
                                    ('" + tSID.Text + "','" + tcID.Text + "','" + tiID.Text + "','" + sDate + "','" + sStart + "','" + sEnd + "','" + cbStartMer.Text + "','" + cbEndMer.Text + "','false','false')", con);
                                cmd.ExecuteNonQuery();

                                //string backupname = @"NAME = N'HVNds-Differential Assign Sched'";
                                //cmd = new SqlCommand(differential + backupname, con);
                                //cmd.ExecuteNonQuery();

                                con.Close();
                                cbAssignFilter.SelectedIndex = 0;
                                loadScheduleAssign();
                                cleartextSchedAssign();
                                loadDataSchedStatCmplt();
                                loadDataStudStatSched();
                                MessageBox.Show("Assigned Successfully!", "Assigned", MessageBoxButton.OK, MessageBoxImage.Information);
                                rerun = false;
                            }
                            else
                            {
                                System.Media.SystemSounds.Hand.Play();
                                Dim.Visibility = System.Windows.Visibility.Visible;
                                gridConflict.Visibility = System.Windows.Visibility.Visible;
                                gridPayCncled.Visibility = System.Windows.Visibility.Visible;
                                gridStatAbsences.Visibility = System.Windows.Visibility.Hidden;
                                gridCam.Visibility = System.Windows.Visibility.Hidden;
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("Total assigned hour already exceeded time left!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    }
                }
                else
                {
                    MessageBox.Show("Time started must be less than time ended.", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            else
            {
                MessageBox.Show("Empty Fields!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void comboBox5_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            tSrchSchedAssign.Clear();
            tSrchAssDate.Clear();
            if (cbAssignFilter.SelectedIndex == 0)
            {
                loadScheduleAssign();
            }
            else
            {
                loadScheduleAssignOverdue();
            }
        }

        private void tSrchSchedAssign_TextChanged(object sender, TextChangedEventArgs e)
        {
            tSrchAssDate.Clear();
            if (cbAssignFilter.SelectedIndex == 0)
            {
                loadScheduleAssign();
            }
            else
            {
                loadScheduleAssignOverdue();
            }
            if (tSrchSchedAssign.Text != "")
            {
                lSrchSchedView.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                lSrchSchedView.Visibility = System.Windows.Visibility.Visible;
            }
        }

        private void tStudentSchedSrch_TextChanged(object sender, TextChangedEventArgs e)
        {
            loadDataStudIns();
            if (tStudentSchedSrch.Text != "")
            {
                lSrchAssignSched.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                lSrchAssignSched.Visibility = System.Windows.Visibility.Visible;
            }
            dataCarSched.ItemsSource = null;
        }

        private void btnNext_Click(object sender, RoutedEventArgs e)
        {
            if (tSrchAssDate.Text == "")
            {
                if (cbAssignFilter.SelectedIndex == 0)
                {
                    GetMaxMinDate();
                }
                if (cbAssignFilter.SelectedIndex == 1)
                {
                    GetMaxMinDateOverdue();
                }
                tSrchAssDate.Text = maxDateFilter;
            }
            else
            {
                DateTime dtnew = DateTime.Parse(tSrchAssDate.Text).AddDays(1);
                tSrchAssDate.Text = dtnew.ToLongDateString();
            }
        }

        private void tSrchAssDate_TextChanged(object sender, TextChangedEventArgs e)
        {
            tSrchSchedAssign.Clear();
            if (tSrchAssDate.Text == "")
            {
                label51.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                if (cbAssignFilter.SelectedIndex == 0)
                {
                    loadScheduleAssignDate();
                }
                else
                {
                    loadScheduleAssignOverdueDate();
                }
                label51.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void btnPrev_Click(object sender, RoutedEventArgs e)
        {
            if (tSrchAssDate.Text == "")
            {
                if (cbAssignFilter.SelectedIndex == 0)
                {
                    GetMaxMinDate();
                }
                if (cbAssignFilter.SelectedIndex == 1)
                {
                    GetMaxMinDateOverdue();
                }
                tSrchAssDate.Text = minDateFilter;

            }
            else
            {

                DateTime dtnew = DateTime.Parse(tSrchAssDate.Text).AddDays(-1);
                tSrchAssDate.Text = dtnew.ToLongDateString() ;
            }
        }

        private void cbStartMer_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            cbEndMer.SelectedIndex = -1;
            if (cbStartMer.SelectedIndex == 1)
            {
                cbEndMer.SelectedIndex = 1;
                cbEndMer.IsHitTestVisible = false;
                cbEnd.IsHitTestVisible = true;
            }
            else
            {
                if (cbStartMer.SelectedIndex != -1)
                {
                    cbEndMer.IsHitTestVisible = true;
                    cbEnd.IsHitTestVisible = true;
                }
                else
                {
                    cbEndMer.IsHitTestVisible = false;
                    cbEnd.IsHitTestVisible = false;
                }
            }
        }

        private void cbStart_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cbStartMer.SelectedIndex != -1)
            {
                cbEndMer.IsHitTestVisible = true;
                cbEnd.IsHitTestVisible = true;
            }
            else
            {
                cbEndMer.IsHitTestVisible = false;
                cbEnd.IsHitTestVisible = false;
            }
        }

        private void btnCompleted_Click(object sender, RoutedEventArgs e)
        {
            if (dataScheduleAssign.SelectedIndex != -1)
            {
                MessageBoxResult mi = MessageBox.Show("Are you sure this assigned schedule is finished?", "Complete?", MessageBoxButton.YesNo, MessageBoxImage.Question);
                if (mi == MessageBoxResult.Yes)
                {
                    con.Open();
                    cmd = new SqlCommand(@"UPDATE SCHEDULE_ASSIGN
                                       SET schedCmplt = 'true', schedStatus='Completed' WHERE schedID = '" + tSchedID.Text + "'", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential AS Completed'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    if (cbAssignFilter.SelectedIndex == 0)
                    {
                        loadScheduleAssign();
                    }
                    else
                    {
                        loadScheduleAssignOverdue();
                    }
                    MessageBox.Show("OK", "Completed", MessageBoxButton.OK, MessageBoxImage.Information);
                    tSchedID.Clear();
                    cleartextSchedAssign();
                    loadDataStudStatSched();
                    dataSchedStatComplt.ItemsSource = null;
                }
            }
            else
            {
                MessageBox.Show("Select from assignments first!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void tSrchStudSched_TextChanged(object sender, TextChangedEventArgs e)
        {
            loadDataStudStatSched();
            tStatTimeLeft.Clear();
            tTimeUsedStat.Clear();
            if (tSrchStudSched.Text == "")
            {
                lSrchStudStat.Visibility = System.Windows.Visibility.Visible;
            }
            else
            {
                lSrchStudStat.Visibility = System.Windows.Visibility.Hidden;
            }
        }

        private void dataStudentsStatus_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (dataStudentsStatus.SelectedIndex != -1)
            {
                GetTotalHoursAssignedComplete();
                int total = int.Parse(tTrainHours.Text) - int.Parse(tTimeUsedStat.Text);
                tStatTimeLeft.Text = total.ToString();
                loadDataSchedStatCmplt();
                dataSchedStatComplt.SelectedIndex = dataSchedStatComplt.Items.Count - 1;
                if (dataSchedStatComplt.Items.Count == 0)
                {
                    btnMovetoAssign.IsEnabled = false;
                    btnRemoveSchedStud.IsEnabled = false;
                }
                else
                {
                    btnMovetoAssign.IsEnabled = true;
                    btnRemoveSchedStud.IsEnabled = true;
                }
                if (tStatTimeLeft.Text == "0")
                {
                    tStatStat.Text = "Completed";
                }
                else
                {
                    tStatStat.Text = "Not Completed";
                }
            }
        }

        private void btnNoShow_Click(object sender, RoutedEventArgs e)
        {
            if (dataScheduleAssign.SelectedIndex != -1)
            {
                MessageBoxResult mi = MessageBox.Show("Move to Absences?","No Show",MessageBoxButton.YesNo,MessageBoxImage.Question);
                if (mi == MessageBoxResult.Yes)
                {
                    con.Open();
                    cmd = new SqlCommand(@"UPDATE SCHEDULE_ASSIGN
                                       SET schedNoShow = 'true' WHERE schedID = '" + tSchedID.Text + "'", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential AS No Show'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    if (cbAssignFilter.SelectedIndex == 0)
                    {
                        loadScheduleAssign();
                    }
                    else
                    {
                        loadScheduleAssignOverdue();
                    }
                    MessageBox.Show("Moved to Absences!", "No Show", MessageBoxButton.OK, MessageBoxImage.Information);
                    tSchedID.Clear();
                    cleartextSchedAssign();
                    loadDataStudStatSched();
                    dataSchedStatComplt.ItemsSource = null;
                }
            }
            else
            {
                MessageBox.Show("Select from students first!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnCancelAssign_Click(object sender, RoutedEventArgs e)
        {
            if (dataScheduleAssign.SelectedIndex != -1)
            {
                MessageBoxResult mi = MessageBox.Show("Cancel Assignment?", "Cancel?", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (mi == MessageBoxResult.Yes)
                {
                    con.Open();
                    cmd = new SqlCommand(@"DELETE FROM SCHEDULE_ASSIGN
                                       WHERE schedID = '" + tSchedID.Text + "'", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential AS Cancel'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    if (cbAssignFilter.SelectedIndex == 0)
                    {
                        loadScheduleAssign();
                    }
                    else
                    {
                        loadScheduleAssignOverdue();
                    }
                    MessageBox.Show("Canceled!", "Canceled!", MessageBoxButton.OK, MessageBoxImage.Information);
                    tSchedID.Clear();
                    cleartextSchedAssign();
                }
            }
            else
            {
                MessageBox.Show("Select from assignments first!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        private void btnMovetoAssign_Click(object sender, RoutedEventArgs e)
        {
            int selIn = dataStudentsStatus.SelectedIndex;
            MessageBoxResult mi = MessageBox.Show("Move to Assignments?", "Move?", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (mi == MessageBoxResult.Yes)
            {
                con.Open();
                cmd = new SqlCommand(@"UPDATE SCHEDULE_ASSIGN
                                       SET schedCmplt = 'false' WHERE schedID = '" + tSchedStatID.Text + "'", con);
                cmd.ExecuteNonQuery();

                //string backupname = @"NAME = N'HVNds-Differential ST Moved to Assignments'";
                //cmd = new SqlCommand(differential + backupname, con);
                //cmd.ExecuteNonQuery();

                con.Close();
                if (cbAssignFilter.SelectedIndex == 0)
                {
                    loadScheduleAssign();
                }
                else
                {
                    loadScheduleAssignOverdue();
                }
                MessageBox.Show("Moved!", "Move", MessageBoxButton.OK, MessageBoxImage.Information);
                tSchedID.Clear();
                cleartextSchedAssign();
                loadDataSchedStatCmplt();
                loadDataStudStatSched();
                dataStudentsStatus.SelectedIndex = selIn;
            }
            
        }

        private void btnRemoveSchedStud_Click(object sender, RoutedEventArgs e)
        {
            if (dataStudentsStatus.SelectedIndex != -1)
            {
                int selIn = dataStudentsStatus.SelectedIndex;
                MessageBoxResult mi = MessageBox.Show("Remove latest schedule record of student?", "Remove?", MessageBoxButton.YesNo, MessageBoxImage.Warning);
                if (mi == MessageBoxResult.Yes)
                {
                    con.Open();
                    cmd = new SqlCommand(@"DELETE FROM SCHEDULE_ASSIGN
                                           WHERE schedID = '" + tSchedStatID.Text + "'", con);
                    cmd.ExecuteNonQuery();

                    //string backupname = @"NAME = N'HVNds-Differential ST Remove'";
                    //cmd = new SqlCommand(differential + backupname, con);
                    //cmd.ExecuteNonQuery();

                    con.Close();
                    if (cbAssignFilter.SelectedIndex == 0)
                    {
                        loadScheduleAssign();
                    }
                    else
                    {
                        loadScheduleAssignOverdue();
                    }
                    MessageBox.Show("Removed!", "Remove", MessageBoxButton.OK, MessageBoxImage.Information);
                    tSchedID.Clear();
                    cleartextSchedAssign();
                    loadDataSchedStatCmplt();
                    loadDataStudStatSched();
                    dataStudentsStatus.SelectedIndex = selIn;
                }
            }
            else
            {
                MessageBox.Show("Select from student first!", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        private void btnAbsencesStat_Click(object sender, RoutedEventArgs e)
        {
            loadDataSchedAbsences();
            Dim.Visibility = System.Windows.Visibility.Visible;
            gridConflict.Visibility = System.Windows.Visibility.Visible;
            gridPayCncled.Visibility = System.Windows.Visibility.Visible;
            gridStatAbsences.Visibility = System.Windows.Visibility.Visible;
            gridCam.Visibility = System.Windows.Visibility.Hidden;
        }
        #region CameraCodes
        private void btnSImage_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Dim.Visibility = System.Windows.Visibility.Visible;
                gridConflict.Visibility = System.Windows.Visibility.Hidden;
                gridPayCncled.Visibility = System.Windows.Visibility.Hidden;
                gridStatAbsences.Visibility = System.Windows.Visibility.Hidden;
                gridCam.Visibility = System.Windows.Visibility.Visible;
                clickedPanel = 1;
                loadCamera();

            }
            if (e.RightButton == MouseButtonState.Pressed)
            {
                Microsoft.Win32.OpenFileDialog openfd = new Microsoft.Win32.OpenFileDialog();
                openfd.Filter = "Images |* .jpg; *.jepeg; *.png; *.gif;";
                openfd.ShowDialog();
                if (openfd.FileName.Count() != 0)
                {
                    iSpp.Source = new BitmapImage(new Uri(openfd.FileName));

                }
            }
        }
        FilterInfoCollection webcam;
        VideoCaptureDevice cam;
        private void loadCamera()
        {
            webcam = new FilterInfoCollection(FilterCategory.VideoInputDevice);
            foreach (FilterInfo VideoCaptureDevice in webcam)
            {
                cbcam.Items.Add(VideoCaptureDevice.Name);

            }
            if (cbcam.Items.Count != 0)
            {
                cbcam.SelectedIndex = 0;
                lErr.Visibility = System.Windows.Visibility.Hidden;
                camerastart();
            }
            else
            {
                lErr.Visibility = System.Windows.Visibility.Visible;
                btnTakePic.IsEnabled = false;
                btnSavePic.IsEnabled = false;
                btnReTakePic.IsEnabled = false;
            }
        }
        private void camerastart()
        {
            cam = new VideoCaptureDevice(webcam[cbcam.SelectedIndex].MonikerString);
            cam.NewFrame += new NewFrameEventHandler(cam_NewFrame);
            cam.Start();
        }
        void cam_NewFrame(object sender, NewFrameEventArgs eventArgs)
        {
            System.Drawing.Image img = (Bitmap)eventArgs.Frame.Clone();

            MemoryStream ms = new MemoryStream();
            img.Save(ms, ImageFormat.Bmp);
            ms.Seek(0, SeekOrigin.Begin);
            BitmapImage bit = new BitmapImage();
            bit.BeginInit();
            bit.StreamSource = ms;
            bit.EndInit();

            bit.Freeze();
            Dispatcher.BeginInvoke(new ThreadStart(delegate
            {
                icam.Source = bit;
            }));

        }

        private void btnTakePic_Click(object sender, RoutedEventArgs e)
        {
            btnSavePic.IsEnabled = true;
            btnTakePic.Visibility = System.Windows.Visibility.Hidden;
            if (cam.IsRunning)
            {
                cam.Stop();
            }
        }
        int clickedPanel;
        private void btnSavePic_Click(object sender, RoutedEventArgs e)
        {
            string filenme = "";
            string fullfilnme = "";
            //"C:\HVN_profile_pics\" + filenme + ".jpg"
            if (fullfilnme == "")
            {
                filenme = "1";
                if (File.Exists(@"C:\HVN_profile_pics\" + filenme + ".jpg"))
                {
                    bool lp = true;
                    int increment = int.Parse(filenme);
                    while (lp)
                    {
                        increment++;
                        if (!File.Exists(@"C:\HVN_profile_pics\" + increment.ToString() + ".jpg"))
                        {
                            filenme = increment.ToString();
                            lp = false;
                        }
                    }

                }
                fullfilnme = @"C:\DS_profile_pics\" + filenme + ".jpg";
            }

            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create((BitmapSource)icam.Source));
            using (FileStream stream = new FileStream(fullfilnme, FileMode.Create))
            encoder.Save(stream);

            ImageSource imgsource = new BitmapImage(new Uri(fullfilnme));
            if (clickedPanel == 1)
            {
                iSpp.Source = imgsource;
            }
            if (clickedPanel == 2)
            {
                iIpp.Source = imgsource;
            }
     
            clickedPanel = 0;
            camclose.RaiseEvent(new RoutedEventArgs(System.Windows.Controls.Primitives.ButtonBase.ClickEvent));
            filenme = "";
            fullfilnme = "";
            icam.Source = null;
        }

        private void camclose_Click(object sender, RoutedEventArgs e)
        {
            if (cbcam.Items.Count != 0)
            {
                if (cam.IsRunning)
                {
                    cam.Stop();
                }
            }
            btnSavePic.IsEnabled = false;
            btnTakePic.Visibility = System.Windows.Visibility.Visible;
            icam.Source = null;
            Dim.Visibility = System.Windows.Visibility.Collapsed;
        }

        private void btniPic_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
            {
                Dim.Visibility = System.Windows.Visibility.Visible;
                gridConflict.Visibility = System.Windows.Visibility.Hidden;
                gridPayCncled.Visibility = System.Windows.Visibility.Hidden;
                gridStatAbsences.Visibility = System.Windows.Visibility.Hidden;
                gridCam.Visibility = System.Windows.Visibility.Visible;
                clickedPanel = 2;
                loadCamera();

            }
            if (e.RightButton == MouseButtonState.Pressed)
            {
                Microsoft.Win32.OpenFileDialog openfd = new Microsoft.Win32.OpenFileDialog();
                openfd.Filter = "Images |* .jpg; *.jepeg; *.png; *.gif;";
                openfd.ShowDialog();
                if (openfd.FileName.Count() != 0)
                {
                    iIpp.Source = new BitmapImage(new Uri(openfd.FileName));

                }
            }
        }

        private void btnReTakePic_Click(object sender, RoutedEventArgs e)
        {
            camerastart();
            btnTakePic.Visibility = System.Windows.Visibility.Visible;
            btnSavePic.IsEnabled = false;
        }
        #endregion

        private void printStudentInfo()
        {
            // Create a PrintDialog
            PrintDialog printDlg = new PrintDialog();

            // Create a FlowDocument dynamically.
            FlowDocument doc = CreateFlowDocument();
            doc.Name = "FlowDoc";

            // Create IDocumentPaginatorSource from FlowDocument
            IDocumentPaginatorSource idpSource = doc;

            if (printDlg.ShowDialog() == true)
            {

                // Call PrintDocument method to send document to printer
                printDlg.PrintDocument(idpSource.DocumentPaginator, "Hello WPF Printing.");
                
            }
        }

        private FlowDocument CreateFlowDocument()
        {
            FlowDocument doc = new FlowDocument();

            Section sec = new Section();

            Paragraph par = new Paragraph();

            Run title1 = new Run("HVN");
            Run title2 = new Run("Driving School");

            par.TextAlignment = TextAlignment.Center;
            par.Inlines.Add(title1);
            par.Inlines.Add(new LineBreak());
            par.Inlines.Add(title2);

            sec.Blocks.Add(par);

            doc.Blocks.Add(sec);

            return doc;
        }

        private void btnPrintStud_Click(object sender, RoutedEventArgs e)
        {
            if (tID.Text != "")
            {
                PrintStud print = new PrintStud();
           
                print.rRegNo.Text = tID.Text;
                print.rName.Text = tLname.Text + ", " + tFname.Text + " " + tMI.Text;
                print.rBday.Text = tBday.Text;
                print.rSex.Text = tSex.Text;
                print.rStat.Text = tStat.Text;
                print.rOcc.Text = tOcc.Text;
                print.rConum.Text = tConum.Text;
                print.rCtzn.Text = tCtzn.Text;
                print.rAdrs.Text = tAdrs.Text;
                print.rpName.Text = tpName.Text;
                print.rpAdrs.Text = tpAdrs.Text;
                print.rpConum.Text = tpConum.Text;
                print.rDatefill.Text = tDateFill.Text;
                print.rTrans.Text = tTrans.Text;
                print.rFee.Text = tFee.Text;
                print.rHrs.Text = tHrs.Text;
                print.rAge.Text = tAge.Text;
                print.ppstud.Source = iSpp.Source;
                print.rpRel.Text = tRel.Text;
                print.ShowDialog();
            }
            else
            {
                MessageBox.Show("Select from student first","Error",MessageBoxButton.OK,MessageBoxImage.Error);
            }
            

        }

        private void btnPrintPay_Click(object sender, RoutedEventArgs e)
        {
            Print print = new Print();
            print.dataTransactions.ItemsSource = dataTransactions.ItemsSource;
            print.textBox1.Text = lID.Text;
            print.textBox2.Text = lName.Text;
            print.textBox3.Text = lFee.Text;
            print.textBox4.Text = tPartial.Text;
            print.textBox5.Text = tBalance.Text;
            print.textBox6.Text = tpayStat.Text;
            print.dataTransactions.Height = (dataTransactions.RowHeight * dataTransactions.Items.Count);
            print.ShowDialog();
            
            
        }

        private void btnPrintStat_Click(object sender, RoutedEventArgs e)
        {
            PrintSchedStat print = new PrintSchedStat();
            print.dataSchedStatComplt.ItemsSource = dataSchedStatComplt.ItemsSource;
            print.textBox1.Text = tStudStatID.Text;
            print.textBox2.Text = textBox4.Text;
            print.textBox3.Text = tTrainHours.Text;
            print.textBox4.Text = tTimeUsedStat.Text;
            print.textBox5.Text = tStatTimeLeft.Text;
            print.textBox6.Text = tStatStat.Text;
            print.dataSchedStatComplt.Height = (dataSchedStatComplt.RowHeight * dataSchedStatComplt.Items.Count);
            print.ShowDialog();

        }
        public void TimerHome_Tick(object sender, EventArgs e)
        {
            if (gridHomeSlide.Width == 1225)
            {
                TimerHome.IsEnabled = false;
                Clock.IsEnabled = true;
            }
            else if (gridHomeSlide.Width > 1200)
            {
                gridHomeSlide.Width += 1;
            }
            else
            {
                gridHomeSlide.Width += 15;
            }
            
            
        }

        private void button4_Click_1(object sender, RoutedEventArgs e)
        {
            ldatetoday.Content = DateTime.Now.ToLongDateString() + "   " + DateTime.Now.ToShortTimeString();
 
            if (gridHome.Visibility == System.Windows.Visibility.Hidden)
            {
                loadSchedTodayHome();
                gridHome.Visibility = System.Windows.Visibility.Visible;
                TimerHome.IsEnabled = true;
                redRect.Visibility = System.Windows.Visibility.Hidden;
            }
            else
            {
                gridHomeSlide.Width = 594;
                gridHome.Visibility = System.Windows.Visibility.Hidden;
                redRect.Visibility = System.Windows.Visibility.Visible;
                Clock.IsEnabled = false;
            }
        }
    public void Clock_Tick(object sender, EventArgs e)
    {
        ldatetoday.Content = DateTime.Now.ToLongDateString() + "   " + DateTime.Now.ToShortTimeString(); ;
    }

    private void lTimer_MouseDown(object sender, MouseButtonEventArgs e)
    {
        
    }

    private void tUser_TextChanged(object sender, TextChangedEventArgs e)
    {
        if (tUser.Text == "")
        {
            lUserlog.Visibility = System.Windows.Visibility.Visible;
        }
        else
        {
            lUserlog.Visibility = System.Windows.Visibility.Hidden;
        }
    }

    private void tPass_PasswordChanged(object sender, RoutedEventArgs e)
    {
        if (tPass.Password == "")
        {
            lPasslog.Visibility = System.Windows.Visibility.Visible;
        }
        else
        {
            lPasslog.Visibility = System.Windows.Visibility.Hidden;
        }
    }

    private void btnLogin_Click(object sender, RoutedEventArgs e)
    {
        con.Open();
        cmd = new SqlCommand(@"SELECT passUSer,passCode FROM Login WHERE passUser = '" + tUser.Text + "' and passCode = '" + tPass.Password + "';", con);
        SqlDataReader dr;
        dr = cmd.ExecuteReader();
        int count = 0;
        while (dr.Read())
        {
            count += 1;
        }
        con.Close();
        if (count == 1)
        {
            gridLogin.Visibility = System.Windows.Visibility.Hidden;
            btnHome.Visibility = System.Windows.Visibility.Visible;
            iLogoHVN.Visibility = System.Windows.Visibility.Visible;
            btnHome.RaiseEvent(new RoutedEventArgs(System.Windows.Controls.Primitives.ButtonBase.ClickEvent));
        }
        else
        {
            MessageBox.Show("Login Failed","Error",MessageBoxButton.OK,MessageBoxImage.Error);
        }
        
        
    }

    private void tPass_KeyDown(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
        {
            btnLogin.RaiseEvent(new RoutedEventArgs(System.Windows.Controls.Primitives.ButtonBase.ClickEvent));
        }
    }

    private void tSrchAssDate_PreviewMouseDown(object sender, MouseButtonEventArgs e)
    {
        tSrchAssDate.Clear();
        if (cbAssignFilter.SelectedIndex == 0)
        {
            loadScheduleAssign();
        }
        else
        {
            loadScheduleAssignOverdue();
        }
    }

    private void btnForfeit_Click(object sender, RoutedEventArgs e)
    {
        if (dataScheduleAssign.SelectedIndex != -1)
        {
            MessageBoxResult mi = MessageBox.Show("Are you sure this assigned schedule is forfeited?", "Forfeited?", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (mi == MessageBoxResult.Yes)
            {
                con.Open();
                cmd = new SqlCommand(@"UPDATE SCHEDULE_ASSIGN
                                       SET schedCmplt = 'true', schedStatus='Forfeited' WHERE schedID = '" + tSchedID.Text + "'", con);
                cmd.ExecuteNonQuery();

                //string backupname = @"NAME = N'HVNds-Differential AS Forfieted'";
                //cmd = new SqlCommand(differential + backupname, con);
                //cmd.ExecuteNonQuery();

                con.Close();
                if (cbAssignFilter.SelectedIndex == 0)
                {
                    loadScheduleAssign();
                }
                else
                {
                    loadScheduleAssignOverdue();
                }
                MessageBox.Show("OK", "Forfeited", MessageBoxButton.OK, MessageBoxImage.Information);
                tSchedID.Clear();
                cleartextSchedAssign();
                loadDataStudStatSched();
                dataSchedStatComplt.ItemsSource = null;
            }
        }
        else
        {
            MessageBox.Show("Select from assignments first!", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
        }
    }

    private void tFee_TextChanged(object sender, TextChangedEventArgs e)
    {
       
    }

    

   
        





    }
}
