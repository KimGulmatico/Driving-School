﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MainProjectNew
{
    /// <summary>
    /// Interaction logic for PrintStud.xaml
    /// </summary>
    public partial class PrintStud : Window
    {
        public PrintStud()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            PrintDialog dlg = new PrintDialog();
            dlg.ShowDialog();
            IDocumentPaginatorSource idpSource = doc;
            doc.PageWidth = dlg.PrintableAreaWidth;
            doc.PageHeight = dlg.PrintableAreaHeight;
            doc.ColumnWidth = dlg.PrintableAreaWidth;
            dlg.PrintDocument(idpSource.DocumentPaginator, "Student Profile");
            this.Close();
        }
    }
}
