﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MainProjectNew
{
    public partial class PrintForm : Form
    {
        public string pp,studName,tpName,tRegNo,tSex,tAge,tConum,tpConum,tBday,tStat,tAdrs,tpAdrs,lDatefill,tCtzn,tOcc,tBlnce,tTrans,tHrs,tFee;
        public string whatbtn;

        public PrintForm()
        {
            InitializeComponent();
        }

        private void PrintForm_Load(object sender, EventArgs e)
        {
            if(whatbtn == "stud"){printPreviewDialog1.Document = pDoc;}
            printPreviewDialog1.ShowDialog();
            this.Close();
        }
        private void PrintInfo()
        {
            pDlog.Document = pDoc;
            DialogResult result = pDlog.ShowDialog();
            if (result == DialogResult.OK)
            {
                pDoc.Print();
            }

        }

        private void pDoc_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            Graphics graphic = e.Graphics;
            Font font = new Font("Arial", 12);
            float fontHeight = font.GetHeight();
            int startX = 75;
            int startY = 150;
            int offset = 125;

            //Logo

            //System.Drawing.Image imgg = System.Drawing.Image.FromFile(Main.Properties.Resources.LogoHVN);
            e.Graphics.DrawImage(MainProjectNew.Properties.Resources.logo, new Rectangle(startX + 310, startY - 100, 100, 100));

            //Headers

            graphic.DrawString("HVN", new Font("Arial", 40, FontStyle.Bold), new SolidBrush(Color.Black), 365, 100 + 35);
            graphic.DrawString("Driving School", new Font("Arial", 15, FontStyle.Regular), new SolidBrush(Color.Black), 363, 150 + 35);

            //Image

            System.Drawing.Image img = System.Drawing.Image.FromFile(pp);
            //Point loc = new Point(startX+150, startY + offset);
            //e.Graphics.DrawImage(img, loc);
            e.Graphics.DrawImage(img, new Rectangle(startX + 500 - 50, startY + 150 + 10, 250, 250));

            //Student Information
            graphic.DrawString("" + lDatefill+ "", new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.Black), startX + 583, startY - 55 + offset + 30);
            graphic.DrawString("Student Information", new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset + 30);
            graphic.DrawString("       Reg No.: " + tRegNo + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 60 + 20);
            graphic.DrawString("          Name: " + studName + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 90 + 20);
            graphic.DrawString("      Birthday: " + tBday + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 120 + 20);
            graphic.DrawString("           Age: " + tAge + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 150 + 20);
            graphic.DrawString("           Sex:" + tSex + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 180 + 20);
            graphic.DrawString("       Citizen: " + tCtzn + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 210 + 20);
            graphic.DrawString("    Occupation: " + tOcc + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 240 + 20);
            graphic.DrawString("        Status: " + tStat + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 270 + 20);
            graphic.DrawString("    Tell/Cell#: " + tConum + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 300 + 20);
            graphic.DrawString("       Address: " + tAdrs + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 330 + 20);
            //Person Incase of Emergency

            graphic.DrawString("Person Incase of Emergency", new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset + 360 + 30);
            graphic.DrawString("          Name: " + tpName + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 390 + 40 + 10);
            graphic.DrawString("    Tell/Cell#: " + tpConum + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 420 + 40 + 10);
            graphic.DrawString("       Address: " + tpAdrs + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 450 + 40 + 10);

            //Assessment

            graphic.DrawString("Car Rent", new Font("Arial", 18, FontStyle.Bold), new SolidBrush(Color.Black), startX, startY + offset + 480 + 50 + 10);
            graphic.DrawString(" Transmission : " + tTrans + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 510 + 60 + 20);
            graphic.DrawString("         Hours: " + tHrs + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 540 + 60 + 20);
            graphic.DrawString("   Tuition Fee: " + tFee + "", font, new SolidBrush(Color.Black), startX + 20, startY + offset + 570 + 60 + 20);
        }

        private void pDocPay_PrintPage(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
          

        }
    }
}
