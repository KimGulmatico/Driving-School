﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MainProjectNew
{
    /// <summary>
    /// Interaction logic for Print.xaml
    /// </summary>
    public partial class Print : Window
    {
        public Print()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //textBox1.Text = winmain.tID.Text;
            //textBox2.Text = winmain.tLname.Text +", "+ winmain.tFname.Text +" "+winmain.tMI.Text;
            //textBox3.Text = winmain.tBday.Text;
            //textBox4.Text = winmain.tSex.Text;
            printPay();
            this.Close();
            
        }

        private void printPay()
        {
            System.Windows.Controls.PrintDialog Printdlg = new System.Windows.Controls.PrintDialog();
            if ((bool)Printdlg.ShowDialog().GetValueOrDefault())
            {
                Size pageSize = new Size(Printdlg.PrintableAreaWidth, Printdlg.PrintableAreaHeight);
                // sizing of the element.
                grid1.Measure(pageSize);
                grid1.Arrange(new Rect(5, 5, pageSize.Width, pageSize.Height));
                Printdlg.PrintVisual(grid1, Title);
            }
           
        }
    }
}
